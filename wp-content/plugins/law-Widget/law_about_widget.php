<?php
/**
	 * law  about us widget
	 *
	 * Displays Address, Email, Phone
	 *
	 * @author 		Codepassenger
	 * @category 	Widgets
	 * @package 	law/Widgets
	 * @version 	1.0.0
	 * @extends 	WP_Widget
	 */

	class law_address extends WP_Widget{
		public function __construct(){
			parent::__construct('law_address',esc_html__('law Address','law'),array(
			'description' => esc_html__('Set widget show your Address','law'),
			));
		}
		public function widget($args,$instance){
			
			$wp_title = !empty($instance['title'])?$instance['title']:' ';
			$title = $args['before_title'];
			$title .= $instance['title'];
			$title .=$args['after_title'];
			
			$phone = !empty($instance['phone'])?$instance['phone']:' ';
			$address = !empty($instance['address'])?$instance['address']:' ';
			$email = !empty($instance['email'])?$instance['email']:' ';
			
					$contact_add = $args['before_widget'];
						$contact_add .= $title;
						$contact_add .= '<div class="contact-infos">';
							$contact_add .= '<div class="contact-info">';
                                $contact_add .= '<div class="contact-icon">';
                                    $contact_add .= '<i class="icon_pin_alt"></i>';
                                    $contact_add .= '</div>';
                                    
								$contact_add .= '<p class="contact-content">'.$address.'</p>';	
							$contact_add .= '</div>';
							$contact_add .= '<div class="contact-info">';
                                $contact_add .= '<div class="contact-icon">';
                                    $contact_add .= '<i class="icon_phone"></i>';
                                $contact_add .= '</div>';
                                $contact_add .= '<p class="contact-content">'.str_replace(',','</br>',$phone).'</p>';
							$contact_add .= '</div>';	
							$contact_add .= '<div class="contact-info">';
                                $contact_add .= '<div class="contact-icon">';
                                    $contact_add .= '<i class="icon_mail_alt"></i>';
                                $contact_add .= '</div>';
								$contact_add .= '<a class="contact-content" >'.str_replace(',','</br>',$email).'</a>';
							$contact_add .= '</div>';
						$contact_add .= '</div>';
							
					$contact_add .= $args['after_widget'];
					echo $contact_add;
		}
		
		public function form($instance){
			
			$title = isset($instance['title'])?$instance['title']:' ';
			$address = isset($instance['address'])?$instance['address']:' ';
			$phone = isset($instance['phone'])?$instance['phone']:' ';
			$email = isset($instance['email'])?$instance['email']:' ';
			$data = array(
				'title' => $title,
				'address' => $address,
				'phone' => $phone,
				'email' => $email
			);

			foreach($data as $key =>$value){
				if($key == 'address'){
					echo '<p><label for="title">'.ucfirst($key).'</label></p>
						<textarea rows="16" cols="30" id="'.$this->get_field_id($key).'" name="'.$this->get_field_name($key).'">
							'.$value.'
						</textarea>';
				}else{
					echo '<p><label for="title">'.ucfirst($key).'</label></p>
						<input type="text" id="'.$this->get_field_id($key).'"  name="'.$this->get_field_name($key).'" value="'.$value.'">';
				}
				
			}
		}
		
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			$instance['address'] = ( ! empty( $new_instance['address'] ) ) ? strip_tags( $new_instance['address'] ) : '';
			$instance['email'] = ( ! empty( $new_instance['email'] ) ) ? strip_tags( $new_instance['email'] ) : '';
			$instance['phone'] = ( ! empty( $new_instance['phone'] ) ) ? strip_tags( $new_instance['phone'] ) : '';
			return $instance;
		}
	}

	
	
	