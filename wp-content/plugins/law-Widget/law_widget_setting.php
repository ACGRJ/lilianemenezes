<?php
/**
 * @category     WordPress_Plugin
 * @package      Law Widget
 * @author       Codepassenger
 * Plugin Name:  Law Custom Widget
 * Author:       Codepassenger
 * Author URI:   http://Codepassenger.com
 * Text Domain:  law
 * **********************************************************************
 */
 if(!defined('LAW_CUSTOM_WIDGET_CHECK')){
	define( 'LAW_CUSTOM_WIDGET_CHECK', in_array( 'law-widget/law_widget_setting.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );
}
 
 

	  
	
	require_once plugin_dir_path( __FILE__ )."law_about_widget.php";
	require_once plugin_dir_path( __FILE__ )."law_aboutus_widget.php";
	require_once plugin_dir_path( __FILE__ )."law_contact_widget.php";
		if(!function_exists('law_widget_register')):
			function law_widget_register(){
				register_widget('law_contact');
				register_widget('law_address');
				register_widget('law_about');

			}
			add_action('widgets_init','law_widget_register');
		endif;


