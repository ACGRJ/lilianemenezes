<?php
	
	/**
	 * law  Contact widget
	 *
	 * Display Address,Phone,Email,social link address
	 *
	 * @author 		Codepassenger
	 * @category 	Widgets
	 * @package 	law/Widgets
	 * @version 	1.0.0
	 * @extends 	WP_Widget
	 */
	class law_contact  extends WP_Widget{
		public function __construct(){
			parent::__construct('law_contact',esc_html__('Law Contact','law'),array(
			'description' => esc_html__('Contact information','law'),
			));
		}
		public function widget($args,$instance){

			
					$wi_title = !empty($instance['title'])?$instance['title']:'Title';
					$title = $args['before_title'];
					$title .= $wi_title;
					$title .=$args['after_title'];
					
					print $title;
					$social = !empty($instance['social'])?$instance['social']:'1';
					$telephone = !empty($instance['telephone'])?$instance['telephone']:' ';
					$email = !empty($instance['email'])?$instance['email']:' ';
				
					echo '<p>'.esc_html__('Tel:','law').'<strong>'.esc_html($telephone).'</strong></p>';
					echo '<p>'.esc_html__('Email:','law').'<small>'.esc_html($email).'</small></p>';


					if($social  == 1){
						
						echo law_social_link('law-social-link','social-icons');
					}
					
			
		}
		
		public function form($instance){
			
			$title  = isset($instance['title'])? $instance['title']:' ';
			$telephone  = isset($instance['telephone'])? $instance['telephone']:' ';
			$email  = isset($instance['email'])? $instance['email']:' ';
			$social  = isset($instance['social'])? $instance['social']:' ';

			?>
			<p>
				<label for="title"><?php esc_html_e('Title:','law'); ?></label>
			</p>
			<input type="text" id="<?php echo esc_attr($this->get_field_id('title')); ?>"  name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($title); ?>">
			<p>
				<label for="posts"><?php esc_html_e('Telephone Number:','law'); ?></label>
			</p>
			<input type="text" id="<?php echo esc_attr($this->get_field_id('telephone')); ?>"  name="<?php echo esc_attr($this->get_field_name('telephone')); ?>" value="<?php echo esc_attr($telephone); ?>">
			<p>
				<label for="posts"><?php esc_html_e('Email:','law'); ?></label>
			</p>
			<input type="text" id="<?php echo esc_attr($this->get_field_id('email')); ?>"  name="<?php echo esc_attr($this->get_field_name('email')); ?>" value="<?php echo esc_attr($email); ?>">
			<p>
				<label for="posts"><?php esc_html_e('Show Social Link:','law'); ?></label>
			</p>
			<select id="<?php echo esc_attr($this->get_field_id('social')); ?>" name="<?php echo esc_attr($this->get_field_name('social')); ?>" >
				<option <?php selected( $social, '1'); ?> value="1"> <?php esc_html_e('show','law') ?></option>
				<option <?php selected( $social, '2'); ?> value="2"> <?php esc_html_e('Hide','law') ?></option>
			</select>
			<?php
		}
		
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			$instance['telephone'] = ( ! empty( $new_instance['telephone'] ) ) ? strip_tags( $new_instance['telephone'] ) : '';
			$instance['email'] = ( ! empty( $new_instance['email'] ) ) ? strip_tags( $new_instance['email'] ) : '';
			$instance['social'] = ( ! empty( $new_instance['social'] ) ) ? strip_tags( $new_instance['social'] ) : '';
			return $instance;
		}
	}
