<?php 
/**
	 * law Description Widget
	 *
	 * Displays Logo And social widget Link
	 *
	 * @author 		Codepassenger
	 * @category 	Widgets
	 * @package 	law/Widgets
	 * @version 	1.0.0
	 * @extends 	WP_Widget
	 */
	
	class law_about extends WP_Widget{
		const VERSION = '4.2.2';
        
		const CUSTOM_IMAGE_SIZE_SLUG = 'tribe_image_widget_custom';

		public function __construct(){
			parent::__construct('law_about',esc_html__('law About Us Widget','law'),array(
				'description' => esc_html__('law About Us Widget','law'),
			));
			
			add_action( 'sidebar_admin_setup', array( $this, 'admin_setup' ) );
			add_action( 'admin_head-widgets.php', array( $this, 'admin_head' ) );
			
		}
		public function admin_setup() {
			wp_enqueue_media();
			wp_enqueue_script( 'tribe-image-widget', get_template_directory_uri().'/lib/js/image-widget.js', array( 'jquery', 'media-upload', 'media-views' ), self::VERSION );
			wp_localize_script( 'tribe-image-widget', 'TribeImageWidget', array(
				'frame_title' => __( 'Select an Image', 'law' ),
				'button_title' => __( 'Insert Into Widget', 'law' ),
			) );
		}
		private static function get_defaults() {

			$defaults = array(
				'title' => '',
				'image' => 0, // reverse compatible - now attachement_id
				'imageurl' => '', // reverse compatible.
				'attachment_id' => 0, // reverse compatible.
			);
			return $defaults;
		}



		public function form($instance){
			$instance = wp_parse_args( (array) $instance, self::get_defaults() );
			$id_prefix = $this->get_field_id('');
			$title  = isset($instance['title'])? $instance['title']:' ';
			$s_desc  = isset($instance['s_desc'])? $instance['s_desc']:'';
			$facebook  = isset($instance['facebook'])? $instance['facebook']:' ';
			$twitter  = isset($instance['twitter'])? $instance['twitter']:' ';
			$dribbble  = isset($instance['dribbble'])? $instance['dribbble']:' ';
			$vimo  = isset($instance['vimo'])? $instance['vimo']:' ';
			$pinterest  = isset($instance['pinterest'])? $instance['pinterest']:' ';
			
			?>
			<p>
				<label for="title"><?php esc_html_e('Title:','law'); ?></label>
			</p>
			<input type="text" id="<?php echo esc_attr($this->get_field_id('title')); ?>"  name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($title); ?>">
			<div class="uploader">
				<input type="submit" class="button" name="<?php echo $this->get_field_name('uploader_button'); ?>" id="<?php echo $this->get_field_id('uploader_button'); ?>" value="<?php _e('Select an Image', 'law'); ?>" onclick="rianaWidget.uploader( '<?php echo $this->id; ?>', '<?php echo $id_prefix; ?>' ); return false;" />
				<div class="tribe_preview" id="<?php echo $this->get_field_id('preview'); ?>">
					<?php echo $this->get_image_html($instance, false); ?>
				</div>
				<input type="hidden" id="<?php echo $this->get_field_id('attachment_id'); ?>" name="<?php echo $this->get_field_name('attachment_id'); ?>" value="<?php echo abs($instance['attachment_id']); ?>" />
				<input type="hidden" id="<?php echo $this->get_field_id('imageurl'); ?>" name="<?php echo $this->get_field_name('imageurl'); ?>" value="<?php echo $instance['imageurl']; ?>" />
			</div>
			<p>
				<label for="title"><?php esc_html_e('Short Description:','law'); ?></label>
			</p>
			<textarea class="widefat" rows="16" cols="20" id="<?php echo esc_attr($this->get_field_id('s_desc')); ?>" value="<?php echo esc_attr($s_desc); ?>" name="<?php echo esc_attr($this->get_field_name('s_desc')); ?>"><?php echo esc_attr($s_desc); ?></textarea>
			<p>
				<label for="title"><?php esc_html_e('Facebook Link:','law'); ?></label>
			</p>
			<input type="text" id="<?php echo esc_attr($this->get_field_id('facebook')); ?>"  name="<?php echo esc_attr($this->get_field_name('facebook')); ?>" value="<?php echo esc_attr($facebook); ?>">
			<p>
				<label for="title"><?php esc_html_e('Twitter Link:','law'); ?></label>
			</p>
			<input type="text" id="<?php echo esc_attr($this->get_field_id('twitter')); ?>"  name="<?php echo esc_attr($this->get_field_name('twitter')); ?>" value="<?php echo esc_attr($twitter); ?>">
			<p>
				<label for="title"><?php esc_html_e('pinterest Link:','law'); ?></label>
			</p>
			<input type="text" id="<?php echo esc_attr($this->get_field_id('pinterest')); ?>"  name="<?php echo esc_attr($this->get_field_name('pinterest')); ?>" value="<?php echo esc_attr($pinterest); ?>">
			<p>
				<label for="title"><?php esc_html_e('vimeo Link:','law'); ?></label>
			</p>
			<input type="text" id="<?php echo esc_attr($this->get_field_id('vimo')); ?>"  name="<?php echo esc_attr($this->get_field_name('vimo')); ?>" value="<?php echo esc_attr($vimo); ?>">
			<p>
				<label for="title"><?php esc_html_e('Dribbble Link:','law'); ?></label>
			</p>
			<input type="text" id="<?php echo esc_attr($this->get_field_id('dribbble')); ?>"  name="<?php echo esc_attr($this->get_field_name('dribbble')); ?>" value="<?php echo esc_attr($dribbble); ?>">
			<?php
		}
		public function widget($args,$instance){

			//$widget='hello';
			$attachment_id = !empty($instance['attachment_id'])?$instance['attachment_id']:'';
			$imageurl = !empty($instance['imageurl'])?$instance['imageurl']:'';
			$title  = !empty($instance['title'])? $instance['title']:' ';
			$s_desc  = !empty($instance['s_desc'])? $instance['s_desc']:' ';
			$facebook  = !empty($instance['facebook'])? $instance['facebook']:'#';
			$twitter  = !empty($instance['twitter'])? $instance['twitter']:'#';
			$dribbble  = !empty($instance['dribbble'])? $instance['dribbble']:'#';
			//echo $dribbble;exit; 
			$vimo  = !empty($instance['vimo'])? $instance['vimo']:'#';
			$pinterest  = !empty($instance['pinterest'])? $instance['pinterest']:'#';
			
			$image = '';
			if(!empty($attachment_id)){
				$url = wp_get_attachment_image_src( $attachment_id, 'img-responsive' );
				$image = $url[0];
			}elseif(!empty($imageurl)){
				$image = $imageurl;
			}
			
		
		?>
		
		<div class="footer-left">
			<a href="<?php echo esc_url(home_url('/'));?>"><img src="<?php echo $image;?>" alt="logo2"></a>
			<p><?php echo $s_desc; ?> </p>

			<ul>
				<li><a href="<?php echo esc_url($facebook,'law'); ?>"><i class="social_facebook"></i></a></li>
				<li><a href="<?php echo esc_url($twitter,'law'); ?>""><i class="social_twitter"></i></a></li>
				<li><a href="<?php echo esc_url($dribbble,'law'); ?>""><i class="social_dribbble"></i></a></li>
				<li><a href="<?php echo esc_url($pinterest,'law'); ?>""><i class="social_pinterest"></i></a></li>
				<li><a href="<?php echo esc_url($vimo,'law'); ?>""><i class="social_vimeo"></i></a></li>
			</ul>

		</div>
		
        <?php
        

		}
		public function admin_head() {
				?>
			<style type="text/css">
				.uploader input.button {
					width: 100%;
					height: 34px;
					line-height: 33px;
					margin-top: 15px;
				}
				.tribe_preview .aligncenter {
					display: block;
					margin-left: auto !important;
					margin-right: auto !important;
				}
				.tribe_preview {
					overflow: hidden;
					max-height: 300px;
				}
				.tribe_preview img {
					margin: 10px 0;
					height: auto;
				}
			</style>
			<?php
		}
		private function get_image_html( $instance, $include_link = true ) {
			// Backwards compatible image display.
			if ( $instance['attachment_id'] == 0 && $instance['image'] > 0 ) {
				$instance['attachment_id'] = $instance['image'];
			}
			$output = '';
			if ( !empty( $instance['imageurl'] ) ) {
				// If all we have is an image src url we can still render an image.
				$src = $instance['imageurl'];
				$output = '<img width="100" height="100" src="'.$src.'" alt="" />';
			} elseif( abs( $instance['attachment_id'] ) > 0 ) {
				$output = wp_get_attachment_image($instance['attachment_id'],array(100,100));
			}
			return $output;
		}

		public function update( $new_instance, $old_instance ) {
			//$instance = $old_instance;
			$new_instance = wp_parse_args( (array) $new_instance, self::get_defaults() );
			$instance['title'] = strip_tags($new_instance['title']);
			$instance['s_desc'] = strip_tags($new_instance['s_desc']);
			$instance['facebook'] = strip_tags($new_instance['facebook']);
			$instance['twitter'] = strip_tags($new_instance['twitter']);
			$instance['pinterest'] = strip_tags($new_instance['pinterest']);
			$instance['dribbble'] = strip_tags($new_instance['dribbble']);
			$instance['vimo'] = strip_tags($new_instance['vimo']);
			// Reverse compatibility with $image, now called $attachement_id
			if ( $new_instance['attachment_id'] > 0 ) {
				$instance['attachment_id'] = abs( $new_instance['attachment_id'] );
			} elseif ( $new_instance['image'] > 0 ) {
				$instance['attachment_id'] = $instance['image'] = abs( $new_instance['image'] );
			}
			$instance['imageurl'] = $new_instance['imageurl']; // deprecated
			return $instance;
		}
	}
	
