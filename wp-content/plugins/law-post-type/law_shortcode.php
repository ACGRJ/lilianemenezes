<?php 
		function portfolio_shortcode($atts){
		   
			global $post;
			extract( shortcode_atts( array(
					'category' =>''
			), $atts, '' ) );
				   
			$q = new WP_Query(
					array(
						'posts_per_page' => '-1',
						'post_type' => 'case_studies'
					)
				);            
			$args = array(
				'post_type' => 'case_studies',
				'posts_per_page' => '-1',
			);
			 
			$case_studies = new WP_Query($args);
			if(is_array($case_studies->posts) && !empty($case_studies->posts)) {
				foreach($case_studies->posts as $gallery_post) {
					$post_taxs = wp_get_post_terms($gallery_post->ID, 
									'case_studies_category', array("fields" => "all")
								);
					if(is_array($post_taxs) && !empty($post_taxs)) {
						foreach($post_taxs as $post_tax) {
							$portfolio_taxs[$post_tax->slug] = $post_tax->name;
						}
					}
				}
			}
			if(is_array($portfolio_taxs) && !empty($portfolio_taxs) && get_post_meta($post->ID, 'pyre_portfolio_filters', true) != 'no'):
				?>            
			 
				<!--Category Filter-->
				<div class="case">
				<div class="container">
				<div class="row">
					<ul class="filture">
						<li><button class="filter" data-filter="all"><?php esc_html_e ('Todos','law') ?></button></li>
						<?php foreach($portfolio_taxs as $portfolio_tax_slug => $portfolio_tax_name): ?>
							<li><button class="filter" data-filter=".<?php echo esc_attr($portfolio_tax_slug); ?>" ><?php echo esc_attr($portfolio_tax_name); ?></button></li>
						<?php endforeach; ?>
					</ul>
				</div>
				<!--End-->
			 
				<?php
			endif; 
			 
			 
			$list = '<div id="something" class="row">';
			while($q->have_posts()) : $q->the_post();
				$idd = get_the_ID();
				//$portfolio_subtitle = get_post_meta($idd, 'portfolio_subtitle', true);
				//$filterr = get_post_meta($idd, 'filterr', true);
				$small_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'law-case-small-thumbnail' );
				$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID ), 'portfolio_large' );
				//$small_image_url = the_post_thumbnail('project_smal_image', array('class' => 'post-thumb'));
						   
				//Get Taxonomy class
						   
				$item_classes = '';
				$item_cats = get_the_terms($post->ID, 'case_studies_category');
				if($item_cats):
					foreach($item_cats as $item_cat) {
						$item_classes .= $item_cat->slug . ' ';
					}
				endif;			   
				$single_link =
				$list .= '    
				<div class="col-sm-3 col-xs-6 mix '.$item_classes.'">
					<div class="case-hover">
						<a href="'.esc_url(get_post_permalink()).'">
							<img src="'.$small_image_url[0].'" class="img-responsive">
							<div class="owl-lasttext">
								<p>'.esc_attr(get_the_title()).'</p>
							</div>
						</a>
					</div>
				</div>';        
			endwhile;
			$list.= '</div></div></div>';
			//wp_reset_query();
			
			
			return $list;
		}
		add_shortcode('case-studies-portfolio', 'portfolio_shortcode');
		
		
		
		
		function key_factors_shortcode($atts,$content = null){
			global $post;
			extract( shortcode_atts( array(
					'show' => 5
			), $atts) );
			$data =utf8_encode('<h3>D�vidas Frequentes<strong class="red">.</strong></h3>');
                                      
			$data.='<div class="panel-group accordion rounded-iconic-accor" id="key-accordion" role="tablist" aria-multiselectable="true">';
                $args = array('post_type' => 'key_factors','posts_per_page'=> esc_attr($show));
				$key_factors = new WP_Query( $args );
				if($key_factors->have_posts()):
					$p = 0;
					while ( $key_factors->have_posts() ) : $key_factors->the_post();
						$in = ($p == 0)?'in':'';
						$collapse = 'collapse'.$p;
						$heading = 'heading_'.$p;

		                $data.='<div class="panel panel-default">';
		                    $data.='<div class="panel-heading" role="tab" id="'.$heading.'">';
		                        $data.='<h4 class="panel-title">';
		                           $data.=' <a role="button" data-toggle="collapse" data-parent="#key-accordion" href="#'.$collapse.'" aria-expanded="true" aria-controls="keycollapseOne">'.get_the_title().'</a>';
		                        $data.='</h4>';
		                   $data.=' </div>';
		                    $data.='<div id="'.$collapse.'" class="panel-collapse collapse '.$in.'" role="tabpanel" aria-labelledby="'.$heading.'">';
		                        $data.='<div class="panel-body">';
		                            $data.= '<p>'.esc_attr(get_the_content()).'</p>';
		                       $data.=' </div>';
		                    $data.='</div>';
		                $data.='</div>';
               			 $p++;
					endwhile;
				endif;
                
            $data.='</div>';			
			return $data;

		}
		add_shortcode('key-factors','key_factors_shortcode');
	