<?php 
if ( !defined('ABSPATH') )
    exit;

/*
  Plugin Name: BBLaw Post Type
  Plugin URI: http://codepassenger.com/
  Description: Post Type Plugin For BBLaw
  Version: 1.0.0
  Author: codepassenger
  Author URI: http://codepassenger.com

 * BBLaw post type Plugin
 * @since BBLAW 1.0
 * @package BBLAW
 */
 
 
 /*--------------------------------------------------------------
 *			Register Slider Post Type
 *-------------------------------------------------------------*/
 
 require_once  'law_shortcode.php';
 
 if(!function_exists('law_slider_post_type')):
function law_slider_post_type(){
	$labels = array( 
		'name'                	=> _x( 'Slider', 'Slider', 'law' ),
		'singular_name'       	=> _x( 'Slider', 'Slider', 'law' ),
		'menu_name'           	=> esc_html__( 'Slider', 'law' ),
		'parent_item_colon'   	=> esc_html__( 'Parent Slider:', 'law' ),
		'all_items'           	=> esc_html__( 'All Sliders', 'law' ),
		'view_item'           	=> esc_html__( 'View Slider', 'law' ),
		'add_new_item'        	=> esc_html__( 'Add New Slider', 'law' ),
		'add_new'             	=> esc_html__( 'New Slider', 'law' ),
		'edit_item'           	=> esc_html__( 'Edit Slider', 'law' ),
		'update_item'         	=> esc_html__( 'Update Slider', 'law' ),
		'search_items'        	=> esc_html__( 'Search Slider', 'law' ),
		'not_found'           	=> esc_html__( 'No article found', 'law' ),
		'not_found_in_trash'  	=> esc_html__( 'No article found in Trash', 'law' )
	);
	
	$args = array(
		'labels'             	=> $labels,
		'public'             	=> true,
		'publicly_queryable' 	=> true,
		'show_in_menu'       	=> true,
		'show_in_admin_bar'   	=> true,
		'can_export'          	=> true,
		'has_archive'        	=> false,
		'hierarchical'       	=> false,
		'menu_icon'             => 'dashicons-format-gallery',
		'supports'           	=> array( 'title','editor','thumbnail')
	);
	register_post_type('slider',$args);
}
add_action('init','law_slider_post_type');
endif;

/*--------------------------------------------------------------
 *			Register Service Post Type
 *-------------------------------------------------------------*/
if(!function_exists('law_service_post_type')):
	function law_service_post_type(){
		$labels = array( 
			'name'                	=> _x( 'Service', 'Service', 'law' ),
			'singular_name'       	=> _x( 'Service', 'Service', 'law' ),
			'menu_name'           	=> esc_html__( 'Service', 'law' ),
			'parent_item_colon'   	=> esc_html__( 'Parent Service:', 'law' ),
			'all_items'           	=> esc_html__( 'All Services', 'law' ),
			'view_item'           	=> esc_html__( 'View Service', 'law' ),
			'add_new_item'        	=> esc_html__( 'Add New Service', 'law' ),
			'add_new'             	=> esc_html__( 'New Service', 'law' ),
			'edit_item'           	=> esc_html__( 'Edit Service', 'law' ),
			'update_item'         	=> esc_html__( 'Update Service', 'law' ),
			'search_items'        	=> esc_html__( 'Search Service', 'law' ),
			'not_found'           	=> esc_html__( 'No article found', 'law' ),
			'not_found_in_trash'  	=> esc_html__( 'No article found in Trash', 'law' )
		);
		
		$args = array(
			'labels'             	=> $labels,
			'public'             	=> true,
			'publicly_queryable' 	=> true,
			'show_in_menu'       	=> true,
			'show_in_admin_bar'   	=> true,
			'can_export'          	=> true,
			'has_archive'        	=> false,
			'hierarchical'       	=> false,
			'menu_icon'             => 'dashicons-visibility',
			'supports'           	=> array( 'title','editor')
		);
		
		register_post_type('service',$args);
	}
	add_action('init','law_service_post_type');
endif;

/*--------------------------------------------------------------
 *			Register Testimonials Post Type
 *-------------------------------------------------------------*/
if(!function_exists('law_testimonials_post_type')):
	function law_testimonials_post_type(){
		$labels = array( 
			'name'                	=> _x( 'Testimonials', 'Testimonials', 'law' ),
			'singular_name'       	=> _x( 'Testimonials', 'Testimonials', 'law' ),
			'menu_name'           	=> esc_html__( 'Testimonials', 'law' ),
			'parent_item_colon'   	=> esc_html__( 'Parent Testimonials:', 'law' ),
			'all_items'           	=> esc_html__( 'All Testimonials', 'law' ),
			'view_item'           	=> esc_html__( 'View Testimonials', 'law' ),
			'add_new_item'        	=> esc_html__( 'Add New Testimonials', 'law' ),
			'add_new'             	=> esc_html__( 'New Testimonials', 'law' ),
			'edit_item'           	=> esc_html__( 'Edit Testimonials', 'law' ),
			'update_item'         	=> esc_html__( 'Update Testimonials', 'law' ),
			'search_items'        	=> esc_html__( 'Search Testimonials', 'law' ),
			'not_found'           	=> esc_html__( 'No article found', 'law' ),
			'not_found_in_trash'  	=> esc_html__( 'No article found in Trash', 'law' )
		);
		
		$args = array(
			'labels'             	=> $labels,
			'public'             	=> true,
			'publicly_queryable' 	=> true,
			'show_in_menu'       	=> true,
			'show_in_admin_bar'   	=> true,
			'can_export'          	=> true,
			'has_archive'        	=> false,
			'hierarchical'       	=> false,
			'menu_icon'             => 'dashicons-format-quote',
			'supports'           	=> array( 'title','editor')
		);
		
		register_post_type('testimonials',$args);
	}
	add_action('init','law_testimonials_post_type');
endif;

/*--------------------------------------------------------------
 *			Register Law Post Type
 *-------------------------------------------------------------*/
if(!function_exists('law_post_type')):
	function law_post_type(){
		$labels = array( 
			'name'                	=> _x( 'Law', 'Law', 'law' ),
			'singular_name'       	=> _x( 'Law', 'Law', 'law' ),
			'menu_name'           	=> esc_html__( 'Law', 'law' ),
			'parent_item_colon'   	=> esc_html__( 'Parent Law:', 'law' ),
			'all_items'           	=> esc_html__( 'All Law', 'law' ),
			'view_item'           	=> esc_html__( 'View Law', 'law' ),
			'add_new_item'        	=> esc_html__( 'Add New Law', 'law' ),
			'add_new'             	=> esc_html__( 'New Law', 'law' ),
			'edit_item'           	=> esc_html__( 'Edit Law', 'law' ),
			'update_item'         	=> esc_html__( 'Update Law', 'law' ),
			'search_items'        	=> esc_html__( 'Search Law', 'law' ),
			'not_found'           	=> esc_html__( 'No article found', 'law' ),
			'not_found_in_trash'  	=> esc_html__( 'No article found in Trash', 'law' )
		);
		
		$args = array(
			'labels'             	=> $labels,
			'public'             	=> true,
			'publicly_queryable' 	=> true,
			'show_in_menu'       	=> true,
			'show_in_admin_bar'   	=> true,
			'can_export'          	=> true,
			'has_archive'        	=> false,
			'hierarchical'       	=> false,
			'menu_icon'             => 'dashicons-visibility',
			'supports'           	=> array( 'title','editor','thumbnail')
		);
		
		register_post_type('law',$args);
	}
	add_action('init','law_post_type');
endif;

/*--------------------------------------------------------------
 *			Register CASE-STUDIES Post Type
 *-------------------------------------------------------------*/
if(!function_exists('law_case_studies_post_type')):
	function law_case_studies_post_type(){
		$labels = array( 
			'name'                	=> _x( 'Case Studies', 'Case Studies', 'law' ),
			'singular_name'       	=> _x( 'Case Studies', 'Case Studies', 'law' ),
			'menu_name'           	=> esc_html__( 'Case Studies', 'law' ),
			'parent_item_colon'   	=> esc_html__( 'Parent Case Studies:', 'law' ),
			'all_items'           	=> esc_html__( 'All Case Studies', 'law' ),
			'view_item'           	=> esc_html__( 'View Case Studies', 'law' ),
			'add_new_item'        	=> esc_html__( 'Add New Case Studies', 'law' ),
			'add_new'             	=> esc_html__( 'New Case Studies', 'law' ),
			'edit_item'           	=> esc_html__( 'Edit Case Studies', 'law' ),
			'update_item'         	=> esc_html__( 'Update Case Studies', 'law' ),
			'search_items'        	=> esc_html__( 'Search Case Studies', 'law' ),
			'not_found'           	=> esc_html__( 'No article found', 'law' ),
			'not_found_in_trash'  	=> esc_html__( 'No article found in Trash', 'law' )
		);
		
		$args = array(
			'labels'             	=> $labels,
			'public'             	=> true,
			'publicly_queryable' 	=> true,
			'show_in_menu'       	=> true,
			'show_in_admin_bar'   	=> true,
			'can_export'          	=> true,
			'has_archive'        	=> true,
			'hierarchical'       	=> true,
			'menu_icon'             => 'dashicons-book',
			'supports'           	=> array( 'title','editor','thumbnail')
		);
		
		register_post_type('case_studies',$args);
	}
	add_action('init','law_case_studies_post_type');
endif;

/*--------------------------------------------------------------
 *			Register CASE-STUDIES Taxonomy
 *-------------------------------------------------------------*/

if(!function_exists('law_case_studies_portfolio')):
	function law_case_studies_portfolio(){
		register_taxonomy(
			'case_studies_category',  
			'case_studies',
				array(
					'hierarchical'          => true,
					'label'                 => esc_html__('Case Studies Category','law'), 
					'query_var'             => true,
					'show_admin_column'     => true,
					'rewrite'               => array(
						'slug'              => 'case-studies-category', 
						'with_front'        => true
					)
				)
		);
	}
	add_action( 'init', 'law_case_studies_portfolio');
endif;

/*--------------------------------------------------------------
 *			Register Key Factor Post Type
 *-------------------------------------------------------------*/
if(!function_exists('law_key_factor_post_type')):
	function law_key_factor_post_type(){
		$labels = array( 
			'name'                	=> _x( 'Key Factors', 'Key Factors', 'law' ),
			'singular_name'       	=> _x( 'Key Factors', 'Key Factors', 'law' ),
			'menu_name'           	=> esc_html__( 'Key Factors', 'law' ),
			'parent_item_colon'   	=> esc_html__( 'Parent Key Factors:', 'law' ),
			'all_items'           	=> esc_html__( 'All Key Factors', 'law' ),
			'view_item'           	=> esc_html__( 'View Key Factors', 'law' ),
			'add_new_item'        	=> esc_html__( 'Add New Key Factors', 'law' ),
			'add_new'             	=> esc_html__( 'New Key Factors', 'law' ),
			'edit_item'           	=> esc_html__( 'Edit Key Factors', 'law' ),
			'update_item'         	=> esc_html__( 'Update Key Factors', 'law' ),
			'search_items'        	=> esc_html__( 'Search Key Factors', 'law' ),
			'not_found'           	=> esc_html__( 'No article found', 'law' ),
			'not_found_in_trash'  	=> esc_html__( 'No article found in Trash', 'law' )
		);
		
		$args = array(
			'labels'             	=> $labels,
			'public'             	=> true,
			'publicly_queryable' 	=> true,
			'show_in_menu'       	=> true,
			'show_in_admin_bar'   	=> true,
			'can_export'          	=> true,
			'has_archive'        	=> true,
			'hierarchical'       	=> true,
			'menu_icon'             => 'dashicons-admin-network',
			'supports'           	=> array( 'title','editor')
		);
		
		register_post_type('key_factors',$args);
	}
	add_action('init','law_key_factor_post_type');
endif;

/*--------------------------------------------------------------
 *			Register Team Post Type
 *-------------------------------------------------------------*/
if(!function_exists('law_team_post_type')):
	function law_team_post_type(){
		$labels = array( 
			'name'                	=> _x( 'Team', 'Team', 'law' ),
			'singular_name'       	=> _x( 'Team', 'Case Studies', 'law' ),
			'menu_name'           	=> esc_html__( 'Team', 'law' ),
			'parent_item_colon'   	=> esc_html__( 'Parent Case Studies:', 'law' ),
			'all_items'           	=> esc_html__( 'All Team', 'law' ),
			'view_item'           	=> esc_html__( 'View Team', 'law' ),
			'add_new_item'        	=> esc_html__( 'Add New Team', 'law' ),
			'add_new'             	=> esc_html__( 'New Team', 'law' ),
			'edit_item'           	=> esc_html__( 'Edit Team', 'law' ),
			'update_item'         	=> esc_html__( 'Update Team', 'law' ),
			'search_items'        	=> esc_html__( 'Search Team', 'law' ),
			'not_found'           	=> esc_html__( 'No article found', 'law' ),
			'not_found_in_trash'  	=> esc_html__( 'No article found in Trash', 'law' )
		);
		
		$args = array(
			'labels'             	=> $labels,
			'public'             	=> true,
			'publicly_queryable' 	=> true,
			'show_in_menu'       	=> true,
			'show_in_admin_bar'   	=> true,
			'can_export'          	=> true,
			'has_archive'        	=> true,
			'hierarchical'       	=> true,
			'menu_icon'             => 'dashicons-universal-access',
			'supports'           	=> array( 'title','editor','thumbnail','comments')
		);
		
		register_post_type('team',$args);
	}
	add_action('init','law_team_post_type');
endif;
/*---------------------------------------------------
Tinymce custom button
----------------------------------------------------*/

if ( ! function_exists( 'lawshortcodes_add_button' ) ) {
	add_action('init', 'lawshortcodes_add_button');  
	function lawshortcodes_add_button() {  
	   if ( current_user_can('edit_posts') &&  current_user_can('edit_pages') )  
	   {  
		 add_filter('mce_external_plugins', 'law_add_plugin');  
		 add_filter('mce_buttons', 'law_register_button');  
	   }  
	} 
}

if ( ! function_exists( 'law_register_button' ) ) {
	function law_register_button($buttons) {
		array_push($buttons, "law_mce_button");
		return $buttons;  
	}  
}

if ( ! function_exists( 'law_add_plugin' ) ) {
	function law_add_plugin($plugin_array) {
		$plugin_array['law_mce_button'] = plugin_dir_url( __FILE__ ) . 'js/shortcodes.js';
		return $plugin_array;  
	}
}

 
 



