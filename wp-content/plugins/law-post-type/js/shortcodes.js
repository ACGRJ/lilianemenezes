(function() {
	tinymce.PluginManager.add('law_mce_button', function( editor, url ) {
		editor.addButton( 'law_mce_button', {
			text: 'Law Shortcodes',
			icon: false,
			type: 'menubutton',
			menu: [
				{
					text: 'Custom Post Types',
					menu: [
						{
							text: 'Case Studies Portfolio',
							onclick: function() {
								editor.windowManager.open( {
									title: 'Insert law Portfolio',
									body: [
										{
											type: 'textbox',
											name: 'max',
											label: 'Maximum number of Portfolio:',
											value: '10'
										},
                                        
									],
									onsubmit: function( e ) {
                                        if(isNaN(e.data.max)) {
                                            editor.windowManager.alert('Maximum number of Portfolio must be a number.');
                                            return false;
                                        }
                                       
										editor.insertContent( '[case-studies-portfolio number="' + e.data.max + '"]');
									}
								});
							}
						},
						{
							text: 'Law Key Factor',
							onclick: function() {
								editor.windowManager.open( {
									title: 'Insert key Factor',
									body: [
										{
											type: 'textbox',
											name: 'max',
											label: 'Maximum number of Kay Factor:',
											value: '5'
										}
   
									],
									onsubmit: function( e ) {
                                        if(isNaN(e.data.max)) {
                                            editor.windowManager.alert('Maximum number of Key factor must be a number.');
                                            return false;
                                        }
										editor.insertContent( '[key-factors show="' + e.data.max + '"]');
									}
								});
							}
						},
                  

					]
				},
				{
					text: 'Others',
					menu: [
                        {
							text: 'Title with border',
							onclick: function() {
								editor.windowManager.open( {
									title: 'Add a title with border',
									body: [
                                        {
                                            type: 'textbox',
                                            name: 'title',
                                            label: 'Your title:'
                                        },
                                        {
											type: 'listbox',
											name: 'level',
											label: 'Header Level:',
											'values': [
												{text: 'Heading 1', value: 'h1'},
												{text: 'Heading 2', value: 'h2'},
                                                {text: 'Heading 3', value: 'h3'},
                                                {text: 'Heading 4', value: 'h4'},
                                                {text: 'Heading 5', value: 'h5'},
                                                {text: 'Heading 6', value: 'h6'}
											]
										}
									],
									onsubmit: function( e ) {
                                            editor.insertContent( '<' + e.data.level + ' class="border">' + e.data.title + '</' + e.data.level + '>');
									}
								});
							}
						},
						{
							text: 'HighLighted Text',
							onclick: function() {
								editor.windowManager.open( {
									title: 'Add Highlighted',
									body: [
                                        {
											type: 'textbox',
											name: 'highlighted',
											label: 'Your Text:',
										}
									],
									onsubmit: function( e ) {
										editor.insertContent( '<p class="law-custom-highlighted">' + e.data.highlighted + '</p>');
									}
								});
							}
						}
					]
				}
			]
		});
	});
})();