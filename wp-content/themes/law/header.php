<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <!-- Meta --> <?php global $law_option; ?>
    
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://d1azc1qln24ryf.cloudfront.net/114779/Socicon/style-cf.css?9ukd8d">
    <link rel="shortcut icon" type="image/x-icon" href="/wp-content/uploads/2018/04/favicon.png">
   <?php wp_head(); ?>
  
</head>

<body <?php body_class(); ?>>
    <!-- Preloader -->
     <?php if(isset($law_option['law-pre-loader']) && $law_option['law-pre-loader'] == true){ ?>
         <div id="sitePreloader">
            <div id="pre-loader">
                <div id="spinner"></div>
            </div>
        </div>
    <?php } ?>
    <!-- Preloader end -->

    <!-- Header start -->
    <header class="main-header" id="main-header">
        <!-- Topbar start -->
       
        <?php if(isset($law_option['topbar']) && $law_option['topbar'] == true){ ?>
	        <div class="topbar">
	            <div class="container">
	                <div class="row">
	                    <div class="col-xs-12 col-md-6 phone-father">

	                        <span class="top-block phone-block"><?php echo isset($law_option['top-contact'])? esc_attr($law_option['top-contact']):'+ 0920 2730 007'; ?></span>
	                        <span class="top-block phone-block"><?php echo isset($law_option['top-email'])? esc_attr($law_option['top-email']):'mail@dmail.com'; ?></span>
	                    </div>
                        
	                    <div class="col-xs-12 col-md-6">
	                    	<?php echo law_social_link(); ?>
	                        
	                    </div>
	                </div>
	            </div>
	        </div>
	    <?php } ?>
        <!-- Topbar end -->

        <!-- Main Nav Start -->
        <div class="main-nav">
            <div class="container">
                <div class="row">
                    <div class="col-sm-2 col-xs-8">
                        <a class="logo" href="<?php echo esc_url(home_url('/')); ?>">
                            <img src="<?php echo law_custom_logo(); ?>" alt="<?php esc_attr_e('logo','law'); ?>">
                        </a>
                    </div>
                    <div class="col-sm-9 col-xs-4 static-xs">

                        <!-- Primary Navigation -->
                        <nav class="navbar navbar-default">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#primary-navigation" aria-expanded="false">
                                    <span class="sr-only"><?php esc_html_e('Toggle navigation','law'); ?></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="primary-navigation">
                            	<ul class="nav navbar-nav">
                            	<?php 
									 wp_nav_menu(array(
										'theme_location'=> 'primary',
										'menu_class' => 'nav navbar-nav',
										'menu_id' => '',
										'container' => '',
										'fallback_cb' => 'law_default_nav_menu',
										'walker' => new wp_bootstrap_navwalker(),
										
									  ));			
								?>
                               </ul> 
                            </div>
                        </nav>

                    </div>
                </div>
            </div>
        </div>
        <!-- Main Nav End -->
    </header>
    <!-- Header end -->
    <?php if(is_front_page() && !is_home()){
          get_template_part('template/slider');
    }else{ ?>

    	<div class="page-header law-page-header">
            <div class="container text-center">
                <h2 class="page-title">
    	            <?php law_custom_heading();	?>
    			</h2>
            </div>
        </div>
    <?php } ?>