<?php get_header(); global $law_option,$post; ?>
<div class="main-wrap section-padding">
	<div class="page-content">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-7">
					<article class="law-item">
						<?php
						if ( have_posts() ) :
							while ( have_posts() ) : the_post();

						?>
						<div class="post-part">
							<?php if(has_post_thumbnail()){
								the_post_thumbnail('law-post-thumbnil', array('class' => 'img-responsive'));
							} ?>
							<h3><?php the_title();  ?><strong class="red">.</strong></h3>

							<p><?php echo get_the_content(); ?></p>
							
						</div>
						
						<a class="btn" href="<?php echo esc_url($_SERVER[HTTP_HOST].'/contact'); ?>"><?php echo esc_attr('Contato'); ?> <i class="arrow_right"></i></a>

						<div class="post-part">
							<div class="row">
								<!-- Practice Area Accordion Start -->
								<div class="col-md-12 col-sm-12">
									<div class="practices">
										<?php
										$service = rwmb_meta('law_service');
										
										if(function_exists('law_team_post_type')){
											echo do_shortcode('[key-factors]'); 
										}

										?>
									</div>
								</div>
								<!--<div class="col-sm-6 main-features">
								<?php
									if(!empty($service)):
								?>
									<h3><?php esc_html_e('Other Services','law'); ?></h3>
									<ul class="rounded-angle">
									<?php
									
										foreach($service as $service_list):
											echo '<li>'.esc_attr($service_list).'</li>';
										endforeach;
									
									?>

									</ul>
								<?php endif; ?>
								</div>
							</div>-->
						</div><?php 
							
							comments_template( '', true );
							

							endwhile;
							endif; 

							?>
						</article>
						
					</div>
					<div class="col-md-4 col-sm-5">
						<div class="side-bar">
							<?php get_sidebar(); ?> 
						</div>
					</div>
				</div>
			</div>
		</div><!-- /.contents --> 
	</div><!-- /.contents --> 
<?php  get_footer(); ?> 