    <article  id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
        <?php if(LAW_METABOX_ACTIVED){
			$link = rwmb_meta( 'codepassenger_link'); 
            echo '<h5>'.$link.'</h5>';
        } ?>
        
        <h3><?php the_title(); ?></h3>
        
        <?php  
            the_content();
			
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'law' ),
				'after'  => '</div>',
			));
			
			if(is_home()){
				 echo '<a href="'.esc_url( get_the_permalink()).'" class="blogread">'. esc_html__("Leia mais","law").'</a>';
			}
        ?>
        <div class="date-no-th">
            <p><?php echo  get_the_date('d');?></p>
            <span><?php echo get_the_date('M'); ?></span>
        </div>
    </article>