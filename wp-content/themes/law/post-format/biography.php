<?php

    $author_bio_avatar_size = apply_filters( 'twentysixteen_author_bio_avatar_size', 76 );
    $description = get_the_author_meta( 'description' ); 
    if($description == !null){
        ?>
        <div class="about-author">
            <h2 class="mrb-40"><?php esc_html_e("About Author","law")?><strong class="red">.</strong></h2></li>
            <div class="author-box">
                <article class="author-body bc-red">
                    <div class="author-meta">

                        <?php echo get_avatar( get_the_author_meta('user_email'), $author_bio_avatar_size,null,null,array('class'=>array('img-shadow')) ); ?>
                       
                    </div>
                
                    <div class="author-metadata">
                        <h3><?php echo get_the_author(); ?></h3>
                    </div>
               
                    <div class="author-content">
                        <p><?php    echo $description;  ?></p>
                    </div>
                   
             </article>
        </div>
    </div>
   
<?php 
    }

    