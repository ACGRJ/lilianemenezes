<?php
    $post_date = get_the_date("d M");
    $date = explode(" ",$post_date);
    if(is_single()) { 
?> 
    <article id="post-<?php the_ID(); ?>" <?php post_class("post-item"); ?> >
        <?php  
            if ( has_post_thumbnail()) {
                echo '<div class="post-thumb">';
                    the_post_thumbnail('law-post-thumbnil', array('class' => 'img-responsive'));
                echo '<div>';
            }
        ?>
        
        <div class="admin-family">
            <span class="icon icon_profile"></span>
            <span><?php the_author(); ?></span>
            <span class="icon icon_heart"></span>
            <span><?php the_category( ","  ); ?></span>
        </div>
        
        <h3 class="listing-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></h3>
        <?php 
            the_content(); 
            if ( has_post_thumbnail()) {
        ?>
            <span class="post-date"><span><?php print esc_html($date[0]); ?></span><span class="post-month"><?php print esc_html($date[1]); ?></span></span>
        <?php } ?>
    </article>
    <?php get_template_part( 'template-parts/biography'); ?>
    <?php 
        wp_link_pages( array(
            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'geo' ),
            'after'  => '</div>',
        ) );
    ?>

   
 <?php }else{ ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class("post-item"); ?>>
     <?php  
            if ( has_post_thumbnail()) {
                echo '<a class="post-thumb" href="'.get_the_permalink().'">';
                    the_post_thumbnail('law-post-thumbnil', array('class' => 'img-responsive'));
                echo '</a>';
            }
        ?>
       
        <h3 class="listing-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
       <?php  the_excerpt(); ?>
        <a href="<?php the_permalink(); ?>" class="blogread"><?php esc_html_e("Leia mais","law") ?></a>
            <span class="post-date"><span><?php print esc_html($date[0]); ?></span><span class="post-month"><?php print esc_html($date[1]); ?></span></span>
    </article>
<?php }
