<?php
    $post_date = get_the_date("d M");
    $date = explode(" ",$post_date);  
   
?> 
<div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-delay="0s" data-wow-duration="1s">
    <div class="member">
        <?php              
                echo '<a class="post-thumb" href="'.get_the_permalink().'">';
                    the_post_thumbnail('law-team-thumbnail', array('class' => 'img-responsive'));
                echo '</a>';            
        ?>
        <div class="member-info">
            <h3><?php the_title(); ?></h3>
            
            <a href="<?php the_permalink(); ?>"><i class="arrow_right"></i></a>
        </div>
    </div>
</div>


