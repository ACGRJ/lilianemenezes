<?php get_header(); ?>
	  
	<div class="main-wrap section-padding">
        <div class="page-content blog-single">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-8">
							<?php 
							//single post
								if ( have_posts() ) {
									while ( have_posts() ) : the_post();
									//echo "yes";
										get_template_part( 'post-format/content', get_post_format());
									endwhile;
								}else{
									
								?>
									<div class="row">
										<div class="col-xs-12" id="search_align">
											<h2 class="title-404" ><?php echo esc_html__('Nenhum Arquivo encontrado : ','law'); ?> <?php echo get_search_query(); ?></h2>
											<p><?php echo esc_html__('Desculpe, mas não encontramos nenhum post com a sua pesquisa.','law'); ?></p>
										</div>
									</div> <!--/.row-->
								<?php
								}
							?>
					</div>
					<div class="col-md-3 col-sm-4">
	   					<div class="side-bar">
							<?php get_sidebar(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /.contents -->        
<?php  get_footer(); ?>