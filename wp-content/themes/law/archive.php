<?php get_header(); ?>
	<div class="main-wrap section-padding">
        <div class="page-content blog-single">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-8">
						<?php

							if ( have_posts() ) :
								while ( have_posts() ) : the_post();
									get_template_part( 'post-format/content-archive' );
								endwhile;
							else:
								get_template_part( 'post-format/content', 'none' );
							endif;
						?>
						<?php law_pagination(); ?>   
						
					</div>
					<div class="col-md-3 col-sm-4">
   						<div class="side-bar">
							<?php get_sidebar(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /.contents --> 

<?php get_footer(); ?>