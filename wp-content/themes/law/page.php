<?php get_header(); ?>

   <div id="page-<?php the_ID(); ?>" <?php post_class('contents'); ?>>
        <div class="container">
            <div class="row">
			    <div class="col-md-8 col-sm-12">
                    <div class="primary">
						<?php 
						
						 while (have_posts()) : the_post();
						 the_content();
						 endwhile;
						?>
					</div>
				</div>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>