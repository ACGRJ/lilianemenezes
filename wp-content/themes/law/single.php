<?php get_header(); ?>  
	<div class="main-wrap section-padding">
        <div class="page-content blog-single">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-7">
						<?php 
							//single post

							if ( have_posts() ) :
								while ( have_posts() ) : the_post();
									get_template_part( 'post-format/content', get_post_format());
								endwhile;
							else:
								get_template_part( 'post-format/content', 'none' );
							endif;
						
							the_post_navigation(array(
								'prev_text'			 => esc_html__( 'Anterior', 'oneplus' ),
								'next_text'			 => esc_html__( 'Próximo', 'oneplus' ),
								'screen_reader_text' => ' '
							)); 
							//commentes
							if(comments_open() || get_comments_number()) {
								comments_template();
							}
						?>
					</div>
					<div class="col-md-4 col-sm-5">
   						<div class="side-bar">
   							<?php get_sidebar(); ?> 
   						</div>
   					</div>
				</div>
			</div>
		</div>
	</div><!-- /.contents -->        
    

<?php  get_footer(); ?>