<?php	
global $law_option;		
$map_key = '';	
$map_latitude = '';	
$map_longitude = '';	
$heading = '';	
$description = '' ;	
$form_title = '';	
if(isset($law_option['map-api-key']) && $law_option['map-api-key'] !=''){		
	$map_key = $law_option['map-api-key'];	
}	
if(isset($law_option['map-latitude']) && $law_option['map-latitude'] !=''){		
	$map_latitude = $law_option['map-latitude'];	
}	
if(isset($law_option['map-longitude']) && $law_option['map-longitude'] !=''){		
	$map_longitude= $law_option['map-longitude'];	
}	
if(isset($law_option['from-page-heading']) && $law_option['from-page-heading'] !=''){		
	$heading = $law_option['from-page-heading'];	
}	
if(isset($law_option['from-desc']) && $law_option['from-desc'] !=''){		
	$description = $law_option['from-desc'];	
}	
?>	
<div class="main-wrap">        
	<div class="page-content">            
		<div class="container">                
			<div class="row">     
				<div class="section-padding">                        
					<div class="col-md-8 col-sm-7">                           
						<div class="post-part address-contact">							                                 
							<h2><?php echo esc_html($heading); ?>								
							</h2>                                
							<?php echo $description; ?>                            
						</div>                                    										
						<?php 																			
							if(LAW_CONTACT_ACTIVED){													
								echo '<div class="post-part">';	
								$contact_form = '';								
								if(isset($law_option['from-field-heading']) && $law_option['from-field-heading'] !=''){
									$form_title = $law_option['from-field-heading'];	
									echo '<h2>'.$form_title.'</h2>';
								}								
								if(isset($law_option['contact-form']) && $law_option['contact-form'] !=''){
									$contact_form = $law_option['contact-form'];									
									echo do_shortcode($contact_form); 								
								}								
								$form_code = $law_option['contact-form'];																									
								echo '</div>';							
							}							
						?>						
					</div>                        
					<div class="col-md-4 col-sm-5">                            
						<div class="side-bar">								
							<?php dynamic_sidebar('contact'); ?>							
						</div>						
					</div>					
				</div>                                   
				<div class="section-padding">                        
					<div id="googleMap"></div>                    
				</div>						
				
			</div>			
		</div>		
	</div>	
</div>				
<script type="text/javascript">	(function($){		
	function initialize() {          
		var mapProp = {            
			center:new google.maps.LatLng(<?php echo $map_latitude;?> ,<?php echo $map_longitude; ?>),            
			zoom:18,            
			mapTypeId:google.maps.MapTypeId.ROADMAP          
		};          
		var map=new google.maps.Map(document.getElementById("googleMap"), mapProp); 
		var marker = new google.maps.Marker({
          position: new google.maps.LatLng(<?php echo $map_latitude;?> ,<?php echo $map_longitude; ?>),
          map: map,
          title: 'Liliane Menezes Advocacia'
        });       
	}        
	google.maps.event.addDomListener(window, 'load', initialize);							
})(jQuery);</script>