<?php
	global $law_option;
	if(LAW_POST_ACTIVED){
		$atts =array();
		
		extract( shortcode_atts( array('category' =>''),$atts, '' ) );
			
		$list = array();
		$gallery_title = new WP_Query(array('post_type' => 'case_studies','posts_per_page' => '-1'));
		
		if(is_array($gallery_title->posts) && !empty($gallery_title->posts)) {
			foreach($gallery_title->posts as $gallery_post) {
				$gallery_taxs = wp_get_post_terms($gallery_post->ID, 'case_studies_category', array("fields" => "all"));
				if(is_array($gallery_taxs) && !empty($gallery_taxs)) {
					foreach($gallery_taxs as $gallery_tax) {
						$list[$gallery_tax->slug] = $gallery_tax->name;
					}
				}
			}
		}
	
?>
	<div class="main-wrap section-padding">
        <div class="page-content">
            <div class="container">
                <div class="cases-wrap">
                    
					<div class="row">
						<ul class="filture">

							<li><button class="filter" data-filter="all"><?php echo esc_html__("Todos","law")?></button></li>
							<?php 

								foreach($list as $key=>$list_name): ?>
									<li><button class="filter" data-filter=".<?php echo  esc_attr($key);?>"><?php  echo esc_html($list_name);?></button></li>
							<?php  endforeach; ?>
						</ul>
		            </div>
			        <div id="case-shorted" class="row">
				        <?php 
							$q = new WP_Query(array('posts_per_page' => '-1','post_type' => 'case_studies')); 
					
							while($q->have_posts()) : $q->the_post(); 

							$small_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array(300,300));

							$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID ),'epoxy-gallery-view');

							$item_classes = '';
							$item_cats = get_the_terms($post->ID, 'case_studies_category','case_studies');
							
							$test = '';

							foreach($item_cats as $data){
								$test .= ' '.$data->slug;
							}
						?>
			          	<div class="col-md-3 col-sm-4 col-xs-6 mix <?php echo $test; ?>">
	                        <a class="case case-hover" href="<?php the_permalink(); ?>">
	                            <img src="<?php print $small_image_url[0]; ?>" alt="Case Image">
	                            <div class="case-content">
	                                <p><?php echo esc_html($post->post_title); ?></p>
	                            </div>
	                        </a>
	                    </div>

	                    <?php endwhile; wp_reset_postdata(); ?>
	                </div>
        		</div>
    		</div><!-- /.case -->
    	</div><!-- /.case -->
    </div><!-- /.case -->
<?php
	}