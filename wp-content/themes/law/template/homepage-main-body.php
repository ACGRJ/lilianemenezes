<?php global $law_option,$post;
if(isset($law_option['introduction']) && $law_option['introduction'] == true){ ?>
	<div class="intro section-padding fantasy-bg">	
		<div class="container">		
			<div class="row">			
				<div class="col-md-8 col-md-offset-2 text-center">	
				<?php	if(isset($law_option['intro-heading']) && $law_option['intro-heading'] !='' ){ 
							printf("<h2> %s <strong class='red'>.</strong></h2>",esc_html($law_option['intro-heading']));			
						}
						if(isset($law_option['intro-descr'])){ 			
							printf("<p>%s</p>",$law_option['intro-descr']);
						}				
						if(isset($law_option['intro-signature'])){
							printf("<img src='%s' alt='signature'/>",esc_attr($law_option['intro-signature']['url']));
						}	?>			
				</div>	
			</div>	
		</div>
	</div>
<?php }
if(isset($law_option['service']) && $law_option['service'] == true && function_exists('law_service_post_type') ){ ?>
	<div class="service section-padding" id="service">
		<div class="container">
			<div class="row">
				<div class="services">
				<?php 	$args = array('post_type' => 'service','posts_per_page'=>4);
						$service = new WP_Query( $args );
						if($service->have_posts()):					
							$point = 0.00;				
							while ( $service->have_posts() ) : $service->the_post();				
								$icon  = rwmb_meta('law_service_icon');				
								$point +=0.6;
								?>
								<div class="col-md-3 col-sm-6">
									<div class="single-service">
										<div class="service-header">
											<div class="service-icon">
												<i class="<?php echo $icon; ?>"></i>
											</div>
											<h3 class="service-title"><?php the_title(); ?></h3>
											<div class="service-body">
												<?php the_content(); ?>
											</div>
										</div>
									</div>				
								</div>			
							<?php endwhile;
						endif; 
				wp_reset_query();?>		
			</div>	
		</div>
	</div>
</div>
<?php }
if(isset($law_option['achievments']) && $law_option['achievments'] ==true ){ ?>
<div class="fact-area bg-style" id="fact-area">	
	<div class="container">		
		<div class="row">			
			<div class="col-sm-9">				
				<div class="row">					
					<div class="facts">						
						<?php if(isset($law_option['first-achievements'])): ?>							
							<div class="col-sm-3">								
								<div class="fact">									
									<h2 class="fact-counter">
										<span class="count">
										<?php echo esc_attr($law_option['first-achievements']['f-achiv-value']); ?>
											
										</span>
									</h2>									
									<p class="fact-name">
										<?php echo (isset($law_option['first-achievements']['f-achiv-name']))?esc_attr($law_option['first-achievements']['f-achiv-name']):''; ?>							
									</p>								
								</div>							
							</div>						
						<?php endif; 						
						if(isset($law_option['second-achievements'])): ?>						
							<div class="col-sm-3">							
								<div class="fact">								
									<h2 class="fact-counter">
										<span class="count">
											<?php echo esc_attr($law_option['second-achievements']['se-achiv-value']); ?>	
										</span>
									</h2>								
									<p class="fact-name">
										<?php echo esc_attr($law_option['second-achievements']['se-achiv-name']); ?>
									</p>						
								</div>						
							</div>					
						<?php endif;					
						if(isset($law_option['third-achievements'])): ?>					
							<div class="col-sm-3">						
								<div class="fact">							
									<h2 class="fact-counter">
										<span class="count">
											<?php echo esc_attr($law_option['third-achievements']['th-achiv-value']); ?>		
										</span>
									</h2>							
									<p class="fact-name">
										<?php echo esc_attr($law_option['third-achievements']['th-achiv-name']); ?>
									</p>						
								</div>					
							</div>				
						<?php endif;				
						if(isset($law_option['fourth-achievements'])): ?>				
							<div class="col-sm-3">					
								<div class="fact">						
									<h2 class="fact-counter">
										<span class="count">98</span>
									</h2>
									<p class="fact-name">Success Rate</p>
								</div>				
							</div>			
						<?php endif; ?>		
					</div>	
				</div>
			</div>
			<div class="col-sm-3">	
				<?php if(isset($law_option['achiev-contact-link'])){ ?>	
					<a class="btn" href="<?php echo esc_url($law_option['achiev-contact-link']); ?>">
						<?php echo esc_html('Contato'); ?>
						<i class="arrow_right"></i>
					</a>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php } 
if(isset($law_option['about']) && $law_option['about'] == true && function_exists('law_post_type')): ?>
<div class="about-laws" id="about-laws">	
	<div class="container">		
		<div class="about-laws-tabs">			
			<?php   $args = array('post_type' => 'law','posts_per_page'=>5);
					$law = new WP_Query( $args );
					if($law->have_posts()):	?>
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<?php $i = 0;
								while ( $law->have_posts() ) : $law->the_post();
									$law_icon  = rwmb_meta('law_cat_icon');
									echo $law_icon;				$title = str_replace(' ','-',get_the_title());
									$title_name = get_the_title();				
									$tab = $title.'-tab';				
									$active = $i==0?'active':' ';				
									echo '<li role="presentation" class="'.esc_attr($active).'"><a href="#'.esc_html($title).'" id="'.esc_attr($tab).'" role="tab" data-toggle="tab" aria-controls="'.esc_html($title).'" aria-expanded="true"><i class="'.$law_icon.'"></i><p>'.esc_html($title_name).'</p></a></li>';
									$i++;				
								endwhile;
							?>			
						</ul>			
						<!-- Tab panes -->			
						<div class="tab-content">				
							<?php	$p = 0;				
								while ( $law->have_posts() ) : $law->the_post();
									$tav_active = $p==0?'active':'';					
									$features = rwmb_meta('law_feature');				
									$tab_id = str_replace(' ','-',get_the_title());				
									$tab_arial = $title.'-tab';				
									?>				
									<div role="tabpanel" class="tab-pane fade in <?php echo $tav_active;?>" id="<?php echo $tab_id;  ?>">		<div class="row">						
											<div class="col-md-4 hidden-sm hidden-xs">
												<?php the_post_thumbnail('law-thumbnail', array('class' => 'img-responsive')); ?>
											</div>
										<div class="col-md-4 col-sm-6">							
											<h3>
												<?php echo esc_attr('About '.get_the_title().' Law'); ?>
												<strong class="red">.
												</strong>
											</h3>							
											<p>
												<?php echo get_the_content(); ?>
											</p>						
										</div>						
										<div class="col-md-4 col-sm-6">							
											<h3>
												<?php echo esc_html__('Main Features','law'); ?>
												<strong class="red">.
												</strong>
											</h3>							
											<ul class="rounded-angle">								
												<?php								
													if(!empty($features)):									
														foreach($features as $features_list):										
															echo '<li>'.esc_attr($features_list).'</li>';
														endforeach;									
													endif;									
												?>								
											</ul>							
										</div>						
									</div>					
								</div>					
							<?php $p++;endwhile; 
						?>  				
					</div>			
				</div>		
			<?php endif; wp_reset_query(); ?>	
		</div>
	</div>
<?php endif; ?>
<!-- =========================CASE-STUDIES============================== -->   
<?php 
if(isset($law_option['case-studies']) && $law_option['case-studies'] == true && function_exists('law_case_studies_post_type')): 	
	$case_studies_heading = esc_html__('Legal Practice','law');	$portfolio = "#";	
	if(isset($law_option['case-studies-heading']) && $law_option['case-studies-heading'] != ''){
		$case_studies_heading = esc_html__($law_option['case-studies-heading'],'law');	
	 }	
	if(isset($law_option['case-studies-portfolio']) && $law_option['case-studies-portfolio'] != ''){		
		$portfolio =  $law_option['case-studies-portfolio'];	
	}	
?>	
<div class="case-studies fantasy-bg section-padding" id="case-studies">		
	<h2 class="text-center section-title"><?php print($case_studies_heading) ; ?>
		<strong class="red">.</strong>
	</h2>		
	<div id="case-study-carousel" class="case-study-carousel owl-carousel">			
		<?php 			
			$case_args = array('post_type' => 'case_studies','posts_per_page'=>-1);			
			$case_studies = new WP_Query( $case_args );			
			if($case_studies->have_posts()):				
				while ( $case_studies->have_posts() ) : $case_studies->the_post();	?>			
					<a class="case" href="<?php the_permalink(); ?>">				
						<?php the_post_thumbnail('law-case-small-thumbnail'); ?>				
						<div class="case-content">					
							<p>
								<?php the_title(); ?>
							</p>				
						</div>			
					</a>			
				<?php endwhile; 			
			endif;			
			wp_reset_query(); 			
		?>		
	</div>		
	<div class="text-center">			
		<a class="section-btn btn" href="<?php echo esc_attr($portfolio); ?>"> 
			<?php echo esc_html__('View All','law'); ?>
			<i class="arrow_right"></i>
		</a>		
	</div>	
</div>
<?php endif; ?>  
<!-- =========================BLOG============================== -->   
<?php 
if(isset($law_option['blog']) && $law_option['blog'] == true && function_exists('law_post_type')){  ?>    	
	<div id="blog" class="blog section-padding">		
		<div class="container">			
			<div class="row">				
				<div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="0s">								
					<?php 
						$post_args = array('post_type' => 'post','posts_per_page'=>1);					
						$blog_post = new WP_Query( $post_args );					
					  	if($blog_post->have_posts()){						
					  		while ( $blog_post->have_posts() ) : $blog_post->the_post(); ?>
					  			<div class="from-blog">								
					  				<h3 class="subsection-title"><?php esc_html_e('From Blog','law'); ?>
					  					<strong class="red">.
					  					</strong>
					  				</h3>								
					  				<a class="blog-thumb" href="<?php the_permalink(); ?>">									
					  					<?php  if( has_post_thumbnail()) {										
					  								the_post_thumbnail('law-post-thumbnil', array('class' => 'img-responsive'));
					  							}									
					  					?>								
					  				</a>								
					  				<h4 class="blog-title-home">
					  					<a href="<?php the_permalink(); ?>">
					  						<?php the_title(); ?>
					  					</a>
					  				</h4>								
					  				<p>
					  					<?php echo substr(get_the_excerpt(),0,100); ?>
					  				</p>								
					  				<?php if( has_post_thumbnail()){
					  						$post_date = get_the_date("d M");									
					  						$date = explode(" ",$post_date);									
							  				?>									
							  				<span class="post-date">
							  					<span>
							  						<?php print esc_html($date[0]); ?>
							  					</span>
							  					<span class="post-month">
							  						<?php print esc_html($date[1]); ?>
							  					</span>
							  				</span>									
					  				<?php } ?>							
					  			</div>				
					  		<?php endwhile; 					
					  	}				
						wp_reset_query(); 
					?>				
				</div>				
				<div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="0.6s">					
					<?php echo do_shortcode('[key-factors]'); ?>				
				</div>				
				<div class="col-md-4 col-sm-12 wow fadeInUp" data-wow-delay="1.2s">					
					<div class="testimonial-area">						
						<h3 class="subsection-title"><?php  esc_html_e('Testimonials','law'); ?>
							<strong class="red">.
							</strong>
						</h3>						
						<div id="testimonial-carousel" class="owl-carousel testimonial-carousel">							
							<?php 
							  $testim_args = array('post_type' => 'testimonials','posts_per_page'=>5);
							  $testim_post = new WP_Query( $testim_args );
							  if($testim_post->have_posts()){									
							  	while ( $testim_post->have_posts() ) { 
							  		$testim_post->the_post();
							  		$img_id = rwmb_meta('law_author_img');									
							  		$url = '';																
							?>														
							  		<div class="testimonial">								
							  			<blockquote>
							  				<?php the_content(); ?>
							  			</blockquote>								
							  			<div class="testimonial-author">									
							  				<div class="author-thumb">									
							  					<?php if(!empty($img_id)){
							  							foreach($img_id as $img){												
							  								$url = $img['url'];												
							  								echo' <img src="'.$url.'" alt="owlimg" width="75" height="75" />';	
							  							}		
							  						}								
							  					?>									
							  				</div>									
							  				<div class="name-text">										
							  					<h3>
							  						<?php echo esc_html(rwmb_meta('law_author_name'));?>
							  					</h3>                                        
							  					<p>
							  						<?php  echo esc_html(rwmb_meta('law_org_name'));?>
							  					</p>									
							  				</div>								
							  			</div>							
							  		</div>							
							  	<?php  } 
							}
						?>						
					</div>					
				</div>				
			</div>				
			<!--  -->									
		</div>		
	</div>	
</div>		
<?php }  ?>
<!-- =========================PROMO============================== --> 
<?php if(isset($law_option['promo']) && $law_option['promo'] == true): ?>   	
    <div class="promo section-padding" id="promo-1">		
    	<div class="container">			
    		<div class="row">				
    			<div class="col-sm-6 col-sm-offset-6">					
    				<?php 
    					$heading = isset($law_option['promo-heading'])?esc_attr($law_option['promo-heading']):'';					
    					echo '<h3>'.esc_html($heading).'</h3>';					
    						if(isset($law_option['promo-descr'])){						
    							echo '<p>'.$promo_img = esc_attr($law_option['promo-descr']).'</p>';					
    						}					
    				?>				
    			</div>			
    		</div>		
    	</div>	
    </div>
<?php endif; ?>
<!-- =========================CLIENTS============================== -->          
<?php if(isset($law_option['client']) && $law_option['client'] == true): ?>	
	<div class="clients">		
		<div class="container">			
			<div id="client-carousel" class="client-carousel owl-carousel">				
				<?php 				
					$logoArr = $law_option['client-logo'];				
					if(isset($logoArr)){					
						$check = array_filter($logoArr[0]);					
						if(!empty($check)){						
							foreach($logoArr as $logo){							
								echo '<a class="client-logo" href="'.esc_url($logo['url']).'"><img src="'.$logo['image'].'" alt="'.$logo['title'].'"></a>';						
							}					
						}				
					}				
				?>			
			</div>		
		</div>	
	</div>	
<?php endif; ?> 
<!-- =========================TEAM============================== -->
<?php if(isset($law_option['team']) && $law_option['team'] == true && function_exists('law_team_post_type')): 	
	$team_heading = esc_html__('Expert Team','law');	
	if(isset($law_option['team-heading']) && $law_option['team-heading'] != ''){		
		$team_heading = esc_attr($law_option['team-heading']);	
	}	
?> 	
	<div class="team section-padding fantasy-bg">		
		<h2 class="section-title text-center">
			<?php print($team_heading); ?>
				<strong class="red">.</strong>
		</h2>		
		<div class="container">			
			<div class="row">				
				<div class="team-members">					
					<?php					
						$team_args = array('post_type' => 'team','posts_per_page'=>-6);					
						$team_post = new WP_Query( $team_args );					
						if($team_post->have_posts()):						
							$team_dely = 0;					
							while ( $team_post->have_posts() ) : $team_post->the_post(); ?>					
								<a href="<?php the_permalink(); ?>"><div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="<?php echo esc_attr($team_dely); ?>s" data-wow-duration="1s">	
									<div class="member">							
										<?php 
											if(has_post_thumbnail()){								
												the_post_thumbnail('full', array('class' => 'img-responsive'));							
											}								
										?>							
										<div class="member-info">								
											<h3>
												<?php the_title(); ?>										
											</h3>								
											<span class="member-doing">
												<?php echo esc_attr(rwmb_meta('law_designation')); ?>											
											</span>								
											<a href="<?php the_permalink(); ?>">
												<i class="arrow_right"></i>
											</a>							
										</div>						
									</div>					
								</div></a>				
							<?php 	endwhile; 					
						endif; 	
					?>				
				</div>			
			</div>		
		</div>	
	</div>
<?php endif; ?>
<!-- =========================CONTACT============================== -->
<?php if(isset($law_option['show-contact']) && $law_option['show-contact'] == true && class_exists( 'WPCF7' )){	
	$contact_heading = esc_html__('Send A Message','law');$contact_shortcode = '';$contact_img = '';
	if(isset($law_option['home-contact-heading']) && $law_option['home-contact-heading'] !=''){	
		$contact_heading = esc_html__($law_option['home-contact-heading'],'law');
	}
	if(isset($law_option['home-contact-shortcode']) && $law_option['home-contact-shortcode'] !=''){	
		$contact_shortcode = $law_option['home-contact-shortcode'];
	}
	if(isset($law_option['home-contact-img']) && $law_option['home-contact-img'] !=''){	
		$contact_img = $law_option['home-contact-img']['url'];
	}?>
	<div class="contact">	
		<div class="container">		
			<h2 class="section-title">
				<?php print($contact_heading); ?>
				<strong class="red">.
				</strong>
			</h2>		
			<div class="row">			
				<div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">				
					<?php      
						echo do_shortcode($contact_shortcode); 				
					?>			
				</div>		
			</div>	
		</div>	
		<div class="contact-image hidden-sm hidden-xs wow fadeInUp" data-wow-delay="1s">		
			<img src="<?php echo esc_attr($contact_img); ?>" alt="contact-right" class="img-responsive" />	
		</div>
	</div>
<?php } ?>