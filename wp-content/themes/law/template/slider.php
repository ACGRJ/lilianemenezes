    
		

	<div class="slider-area" id="slider-area">
        <div id="header-slider" class="owl-carousel header-slider">
        <?php if(LAW_POST_ACTIVED){ ?>
		<?php 
			$args = array('post_type' => 'slider','posts_per_page'=>5);
				$second_title ='';
				$page_name ='';
				$page_link = '';
				$text_content ='';
				$position='';
				$slider = new WP_Query( $args );
				if($slider->have_posts()):
					$p = 1;
					while ( $slider->have_posts() ) : $slider->the_post();
						if(LAW_METABOX_ACTIVED){
							$second_title = rwmb_meta('law_sc_title');
							$page_name = rwmb_meta('law_page_name');
							$page_link = rwmb_meta('law_page_link');
							$text_content = rwmb_meta('law_text_content');
							$position  = $text_content == 1?'text-center':'';
						}
					
		?>
			            <div class="header-slide header-slide-<?php echo $p; ?>">
			                <div class="slide-content">
			                    <div class="container <?php echo esc_attr($position); ?>">
			                        <h1><?php the_title(); ?></h1>
			                        <h3><?php echo esc_attr($second_title); ?></h3>
			                        <a class="btn" href="<?php echo esc_url($page_link); ?>"><?php echo esc_attr($page_name); ?> <i class="arrow_right"></i></a>
			                    </div>
			                </div>
			            </div>
            <?php 
            			$p++; 
            		endwhile; 
            	endif;
            }
            ?>
            
        </div>
    </div>