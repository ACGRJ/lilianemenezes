<?php 
/* 
	Template Name: Full Width 
*/
?>
<?php get_header(); ?>


   <div id="page-<?php the_ID(); ?>" <?php post_class('contents'); ?>>
        <div class="main-wrap section-padding">
        	<div class="page-content blog-single">
            	<div class="container">
                	<div class="row">
						<?php 
						
						 while (have_posts()) : the_post();
							the_content();
						 endwhile;
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>