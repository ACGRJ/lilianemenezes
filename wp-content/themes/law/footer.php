<?php
global $law_option;
	  $desc = null;
	  $copy_right = " ThemeBeer. All rights reserved.";
	  $footer_logo = '';
	if(LAW_REDUX_ACTIVED){
		if(isset($law_option['law-footer-sh-desc']) && $law_option['law-footer-sh-desc'] != ''){
			$desc = $law_option['law-footer-sh-desc'];
		}
		if(isset($law_option['law-copyright']) && $law_option['law-copyright'] != ''){
			$copy_right = $law_option['law-copyright'];
		}
		if(isset($law_option['footer-logo'])){
			$footer_logo = $law_option['footer-logo']['url'];
		}
	}
?>

	<footer>
        <div class="footer-overlay">
            <div class="container">
                <div class="row">
                	<div class="col-md-4 col-sm-12 footer-logo">
                		<img src="<?=$footer_logo?>">
                	</div>
                	<div class="col-md-4 col-sm-12 footer-address">
                        <div class="row">
                        	<?php dynamic_sidebar('footer-second'); ?>
                            <?php echo $desc; ?>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 footer-social">
                        <div class="row">
                            <?php dynamic_sidebar('footer-second'); ?>
                            <?php echo law_social_link_footer(); ?>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>

        <div class="copyright text-center">
            <p><?php echo $copy_right ?></p>
        </div>
    </footer>
</body>

</html>