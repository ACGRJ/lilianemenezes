<?php
/*
Law all custom function 

*/
if(!function_exists('awesome_comment_form_submit_button')){
	function awesome_comment_form_submit_button($button) {
		$button =
			'<button type="submit" class="btn">
				Send
				<strong class="btn-icon">
					<span class="arrow_right"></span>     
				</strong>
			</button>';
			
		return $button;
	}
	add_filter('comment_form_submit_button', 'awesome_comment_form_submit_button');
}

if(!function_exists('law_get_video_id')){
	function law_get_video_id($url){
		$video = parse_url($url);

		switch($video['host']) {
			case 'youtu.be':
			$id = trim($video['path'],'/');
			$src = 'https://www.youtube.com/embed/' . $id;
			break;

			case 'www.youtube.com':
			case 'youtube.com':
			parse_str($video['query'], $query);
			$id = $query['v'];
			$src = 'https://www.youtube.com/embed/' . $id;
			break;

			case 'vimeo.com':
			case 'www.vimeo.com':
			$id = trim($video['path'],'/');
			$src = "http://player.vimeo.com/video/{$id}";
		}

		return $src;
	}
}
if(!function_exists('law_homepage_section_template')){
	//echo "<hello>";exit;
	function law_homepage_section_template() {
		echo '<div class="main-wrap">';
			get_template_part('template/homepage-main-body');
		echo "</div>";
	}
}
if(!function_exists('law_contactpage_section')){
	function law_contactpage_section(){
		get_template_part('template/contact-main-body');
	}
}
if(!function_exists('law_crimepage_section')){
	function law_crimepage_section(){
		get_template_part('template/crime-main-body');
	}
}

if(!function_exists('law_header_topbar')){
	function law_header_topbar(){
		get_template_part('template/header-topbar');
	}
}

if(!function_exists('law_page_footer')){
	function law_page_footer(){
		get_template_part('template/footer-section');
	}
}

if(!function_exists('law_custom_preloader')){
	function law_custom_preloader(){
		global $law_option;
		 if(isset($law_option['law-pre-loader']) && $law_option['law-pre-loader'] == true ){
			echo '<div id="SitePreloader"><div id="pre-loader"><div id="spinner"></div></div></div>';
		}
	}
}

if(!function_exists('law_custom_favicon')){
	function law_custom_favicon(){
		global $law_option;
			if(isset($law_option['law-favicon']) && $law_option['law-favicon'] != '' ){
				$fav_icon = esc_attr($law_option['law-favicon']['url']);
			}else{
				$fav_icon = '#';
			}

			if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
			?>

			<link rel="icon" href="<?php echo esc_attr($fav_icon); ?>">
			<link rel="apple-touch-icon" href="<?php echo esc_attr($fav_icon); ?>">
			<link rel="apple-touch-icon" sizes="72x72" href="<?php echo esc_attr($fav_icon); ?>">
			<link rel="apple-touch-icon" sizes="114x114" href="<?php echo esc_attr($fav_icon); ?>">
			<?php }

	}
}

if(!function_exists('law_custom_logo')){
	function law_custom_logo(){
		global $law_option;
			///check logo is set//
		$logo  = get_template_directory_uri().'/images/logo.png';
		if(LAW_REDUX_ACTIVED){
			if(isset($law_option['law-logo']) && $law_option['law-logo'] != '' ){
				$logo = $law_option['law-logo']['url'];
			}
		}
		return esc_url($logo);

	}
}

if(!function_exists('law_social_link')){
	function law_social_link($id = 'law-social-link',$class = ''){
		global $law_option;
		$social = '';
		
		$class = !empty($class) ? $class : "social top-social";
		
		if(!empty($law_option[$id]) && isset($law_option[$id])):
			$settings = $law_option[$id];

			if(!empty($settings) && count($settings) >= 1):
				$social = '<ul class="'.$class.'">';
				foreach( $settings as $link){

					strtolower($link);
					if(strpos($link,"facebook.com") !==false){
						$social .= "<li><a href=".$link."><i class='social_facebook'></i></a></li>";
					}elseif(strpos($link,"twitter.com") !==false){
						$social .= "<li><a href=".$link. "><i class='social_twitter'></i></a></li>";
					}elseif(strpos($link,"dribbble.com") !==false){
						$social .= "<li><a href=".$link. "><i class='social_dribbble'></i></a></li>";
					}elseif(strpos($link,"pinterest.com") !==false){
						$social .= "<li><a href=".$link. "><i class='social_pinterest'></i></a></li>";
					}elseif(strpos($link,"vimeo.com") !==false){
						$social .= "<li><a href=".$link. "><i class='social_vimeo'></i></a></li>";
					}elseif(strpos($link,"google.com") !==false){
						$social .= "<li><a href=".$link. "><i class='social_googleplus'></i></a></li>";
					}elseif(strpos($link,"instagram.com") !==false){
						$social .= "<li><a href=".$link. "><i class='social_instagram'></i></a></li>";
					}elseif(strpos($link,"linkedin.com") !==false){
						$social .= "<li><a href=".$link. "><i class='social_linkedin'></i></a></li>";
					}elseif(strpos($link,"api.whatsapp.com") !==false){
						$social .= "<li><a href=".$link. "><i class='socicon-whatsapp'></i></a></li>";
					}
				}
				$social .= "</ul>";
			endif;
		endif;
		return $social;
	}
}

//Copia da de cima para alterar a classe e adicionar dinamicamente no rodapé
if(!function_exists('law_social_link_footer')){
	function law_social_link_footer($id = 'law-social-link',$class = ''){
		global $law_option;
		$social = '';
		
		$class = !empty($class) ? $class : "social footer-social";
		
		if(!empty($law_option[$id]) && isset($law_option[$id])):
			$settings = $law_option[$id];

			if(!empty($settings) && count($settings) >= 1):
				$social = '<ul class="'.$class.'">';
				foreach( $settings as $link){

					strtolower($link);
					if(strpos($link,"facebook.com") !==false){
						$social .= "<li><a href=".$link."><i class='social_facebook'></i></a></li>";
					}elseif(strpos($link,"twitter.com") !==false){
						$social .= "<li><a href=".$link. "><i class='social_twitter'></i></a></li>";
					}elseif(strpos($link,"dribbble.com") !==false){
						$social .= "<li><a href=".$link. "><i class='social_dribbble'></i></a></li>";
					}elseif(strpos($link,"pinterest.com") !==false){
						$social .= "<li><a href=".$link. "><i class='social_pinterest'></i></a></li>";
					}elseif(strpos($link,"vimeo.com") !==false){
						$social .= "<li><a href=".$link. "><i class='social_vimeo'></i></a></li>";
					}elseif(strpos($link,"google.com") !==false){
						$social .= "<li><a href=".$link. "><i class='social_googleplus'></i></a></li>";
					}elseif(strpos($link,"instagram.com") !==false){
						$social .= "<li><a href=".$link. "><i class='social_instagram'></i></a></li>";
					}elseif(strpos($link,"linkedin.com") !==false){
						$social .= "<li><a href=".$link. "><i class='social_linkedin'></i></a></li>";
					}elseif(strpos($link,"api.whatsapp.com") !==false){
						$social .= "<li><a href=".$link. "><i class='socicon-whatsapp'></i></a></li>";
					}
				}
				$social .= "</ul>";
			endif;
		endif;
		return $social;
	}
}




	

	if(!function_exists('law_pagination')){
		
		function law_pagination($prev = 'Prev', $next = 'Next', $pages='' ,$args=array('class'=>'')) {
			global $wp_query, $wp_rewrite;
			$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
			if($pages==''){
				global $wp_query;
				 $pages = $wp_query->max_num_pages;
				 if(!$pages)
				 {
					 $pages = 1;
				 }
			}
			$pagination = array(
				'base' => @add_query_arg('paged','%#%'),
				'format' => '',
				'total' => $pages,
				'current' => $current,
				'prev_text' => "&laquo;",
				'next_text' => "&raquo;",
				'type' => 'array'
			);
			if( $wp_rewrite->using_permalinks() )
				$pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );

			if( !empty($wp_query->query_vars['s']) )
				$pagination['add_args'] = array( 's' => get_query_var( 's' ) );
			if(paginate_links( $pagination )!=''){
				$paginations = paginate_links( $pagination );
				echo '<ul class=pagination " '.$args["class"].'">';
					foreach ($paginations as $key => $pg) {
						echo '<li >'.$pg.'</li>';
					}
				echo '</ul>';
			}
		}
	}
	