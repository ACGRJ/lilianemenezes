<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/
 */


add_filter( 'rwmb_meta_boxes', 'law_register_meta_boxes' );

/**
 * Register meta boxes
 *
 * @return void
 */
function law_register_meta_boxes( $meta_boxes )
{
	/**
	 * Prefix of meta keys (optional)
	 * Use underscore (_) at the beginning to make keys hidden
	 * Alt.: You also can make prefix empty to disable it
	 */
	// Better has an underscore as last sign
	$prefix = 'law_';

		// 1st meta box
	$meta_boxes[] = array(
		'id' => 'post-meta-quote',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title' => esc_html__( 'Post Quote Settings', 'law' ),

		// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
		'pages' => array( 'post'),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context' => 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority' => 'high',

		// Auto save: true, false (default). Optional.
		'autosave' => true,

		// List of meta fields
		'fields' => array(
			array(
				// Field name - Will be used as label
				'name'  => esc_html__( 'Qoute Text', 'law' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}qoute",
				'desc'  => esc_html__( 'Write Your Qoute Here', 'law' ),
				'type'  => 'textarea',
				// Default value (optional)
				'std'   => ''
			),
			array(
				// Field name - Will be used as label
				'name'  => esc_html__( 'Qoute Author', 'law' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}qoute_author",
				'desc'  => esc_html__( 'Write Qoute Author or Source', 'law' ),
				'type'  => 'text',
				// Default value (optional)
				'std'   => ''
			)
			
		)
	);




	$meta_boxes[] = array(
		'id' => 'post-meta-link',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title' => esc_html__( 'Post Link Settings', 'law' ),

		// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
		'pages' => array( 'post'),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context' => 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority' => 'high',

		// Auto save: true, false (default). Optional.
		'autosave' => true,

		// List of meta fields
		'fields' => array(
			array(
				// Field name - Will be used as label
				'name'  => esc_html__( 'Link URL', 'law' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}link",
				'desc'  =>esc_html__( 'Write Your Link', 'law' ),
				'type'  => 'text',
				// Default value (optional)
				'std'   => ''
			)
			
		)
	);


	$meta_boxes[] = array(
		'id' => 'post-meta-audio',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title' => esc_html__( 'Post Audio Settings', 'law' ),

		// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
		'pages' => array( 'post'),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context' => 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority' => 'high',

		// Auto save: true, false (default). Optional.
		'autosave' => true,

		// List of meta fields
		'fields' => array(
			array(
				// Field name - Will be used as label
				'name'  => esc_html__( 'Audio Embed Code', 'law' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}audio_code",
				'desc'  => esc_html__( 'Write Your Audio Embed Code Here', 'law' ),
				'type'  => 'textarea',
				// Default value (optional)
				'std'   => ''
			)
			
		)
	);




	$meta_boxes[] = array(
		'id' => 'post-meta-video',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title' => esc_html__( 'Post Video Settings', 'law' ),

		// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
		'pages' => array( 'post'),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context' => 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority' => 'high',

		// Auto save: true, false (default). Optional.
		'autosave' => true,

		// List of meta fields
		'fields' => array(
			array(
				// Field name - Will be used as label
				'name'  => esc_html__( 'Video Embed Code/ID', 'law' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}video",
				'desc'  => esc_html__( 'Write Your Vedio Embed Code/ID Here', 'law' ),
				'type'  => 'textarea',
				// Default value (optional)
				'std'   => ''
			),
			array(
				'name'     => esc_html__( 'Select Vedio Type/Source', 'law' ),
				'id'       => "{$prefix}video_source",
				'type'     => 'select',
				// Array of 'value' => 'Label' pairs for select box
				'options'  => array(
					'1' => esc_html__( 'Embed Code', 'law' ),
					'2' => esc_html__( 'YouTube', 'law' ),
					'3' => esc_html__( 'Vimeo', 'law' ),
				),
				// Select multiple values, optional. Default is false.
				'multiple'    => false,
				'std'         => '1'
			),
			
		)
	);


	$meta_boxes[] = array(
		'id' => 'post-meta-gallery',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title' => esc_html__( 'Post Gallery Settings', 'law' ),

		// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
		'pages' => array( 'post'),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context' => 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority' => 'high',

		// Auto save: true, false (default). Optional.
		'autosave' => true,

		// List of meta fields
		'fields' => array(
			array(
				'name'             => esc_html__( 'Gallery Image Upload', 'law' ),
				'id'               => "{$prefix}gallery_images",
				'type'             => 'image_advanced',
				'max_file_uploads' => 6,
			)			
		)
	);
	
		//Slider
	$meta_boxes[] = array(
		'id' => 'slider-meta-setting',
		'title' => esc_html__( 'Additional Infomation', 'law' ),
		'pages' => array( 'slider'),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields' => array(

			array(
				'name'  => esc_html__( 'Text Content Show', 'law' ),
				'id'    => "{$prefix}text_content",
				'type'  => 'select',
				// Array of 'value' => 'Label' pairs for select box
				'options'  => array(
					'1' => esc_html__( 'Center', 'law' ),
					'2' => esc_html__( 'Left', 'law' ),
				),
			),	
			
			array(
				'name'  => esc_html__( 'Second Title', 'law' ),
				'id'    => "{$prefix}sc_title",
				'type'  => 'text',
				'std'   => ''
			),			

			array(
				'name'  => esc_html__( 'Page Name', 'law' ),
				'id'    => "{$prefix}page_name",
				'type'  => 'text',
				'std'   => ''
			),
			array(
				'name'  => esc_html__( 'Page Link', 'law' ),
				'id'    => "{$prefix}page_link",
				'type'  => 'text',
				'std'   => ''
			),
		)
	);
	
	//Service
	$meta_boxes[] = array(
		'id' => 'service-meta-setting',
		'title' => esc_html__( 'Additional Infomation', 'law' ),
		'pages' => array( 'service'),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields' => array(
			array(
				'name'  => esc_html__( 'Service Icon ', 'law' ),
				'id'    => "{$prefix}service_icon",
				'type'  => 'text',
				'desc' => esc_html__('Elegant Icon','law'),

			),			
		)
	);
	
	
	//testimonials
	$meta_boxes[] = array(
		'id' => 'testimonials-meta-setting',
		'title' => esc_html__( 'Author Info', 'law' ),
		'pages' => array( 'testimonials'),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields' => array(
			array(
				'name'             => esc_html__( 'Name', 'law' ),
				'id'               => "{$prefix}author_name",
				'type'             => 'text',
			),
			array(
				'name'             => esc_html__( 'Author Img', 'law' ),
				'id'               => "{$prefix}author_img",
				'type'             => 'image_advanced',
				'max_file_uploads' => 1,
			),
			array(
				'name'  => esc_html__( 'Origination Name ', 'law' ),
				'id'    => "{$prefix}org_name",
				'type'  => 'text',
			),			
		)
	);
	$meta_boxes[] = array(
		'id' => 'law-meta-setting',
		'title' => esc_html__( 'Features List', 'law' ),
		'pages' => array( 'law'),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields' => array(
			array(
				'name'             => esc_html__( 'Features Name', 'law' ),
				'id'               => "{$prefix}feature",
				'type'             => 'text',
				'clone'   		   =>true,
			),
			array(
				'name'  => esc_html__( 'Law Icon ', 'law' ),
				'id'    => "{$prefix}cat_icon",
				'type'  => 'text',
				'desc' => esc_html__('Elegant Icon','law'),

			),			
		)
	);
	
	
	$meta_boxes[] = array(
		'id' => 'case-studies-meta-setting',
		'title' => esc_html__( 'Case Studies', 'law' ),
		'pages' => array( 'case_studies'),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields' => array(
			array(
				'name'  => esc_html__( 'Service List', 'law' ),
				'id'    => "{$prefix}service",
				'type'  => 'text',
				'clone' =>true,
			),
		)
	);
	
		$meta_boxes[] = array(
		'id' => 'team-studies-meta-setting',
		'title' => esc_html__( 'Team Member', 'law' ),
		'pages' => array( 'team'),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields' => array(
			array(
				'name'             => esc_html__( 'Eduction History', 'law' ),
				'id'               => "{$prefix}eduction",
				'type'             => 'textarea',
			),
			array(
				'name'             => esc_html__( 'Designation ', 'law' ),
				'id'               => "{$prefix}designation",
				'type'             => 'text',
			),
			array(
				'name'             => esc_html__( 'Eduction  Level', 'law' ),
				'id'               => "{$prefix}eduction_level",
				'type'             => 'textarea',
				'clone'            =>true,
				'max_clone'			=>'3'
			),
			array(
				'name'  => esc_html__( 'Service List', 'law' ),
				'id'    => "{$prefix}te_service",
				'type'  => 'text',
				'clone' =>true,
				'max_clone' =>6
			),
		)
	);
	
	return $meta_boxes;
	
}





