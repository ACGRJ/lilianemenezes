<?php

    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "law_option";
	

    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */

    $sampleHTML = '';
    if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
        Redux_Functions::initWpFilesystem();

        global $wp_filesystem;

        $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
    }

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
    $sample_patterns      = array();

    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'img' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        }
    }
    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => 'law_option',
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => esc_html__( 'Law Option','law'),
        'page_title'           => esc_html__( 'Law Option','law'),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => 60,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'system_info'          => false,
        // REMOVE

        //'compiler'             => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    $args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'http://docs.reduxframework.com/',
        'title' => esc_html__( 'Documentation', 'law' ),
    );

    $args['admin_bar_links'][] = array(
        //'id'    => 'redux-support',
        'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
        'title' => esc_html__( 'Support', 'law' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-extensions',
        'href'  => 'reduxframework.com/extensions',
        'title' => esc_html__( 'Extensions', 'law' ),
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
        'title' => 'Visit us on GitHub',
        'icon'  => 'el el-github'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://twitter.com/reduxframework',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://www.linkedin.com/company/redux-framework',
        'title' => 'Find us on LinkedIn',
        'icon'  => 'el el-linkedin'
    );

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( esc_html__( ' ', 'law' ), $v );
    } else {
        $args['intro_text'] = esc_html__( ' ', 'law' );
    }

    // Add content after the form.
    $args['footer_text'] = esc_html__( ' ', 'law' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => esc_html__( 'Theme Information 1', 'law' ),
            'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'law' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => esc_html__( 'Theme Information 2', 'law' ),
            'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'law' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'law' );
    Redux::setHelpSidebar( $opt_name, $content );


    // -> START Home Fields

    Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'General Setting', 'law'),
        'id'    => 'general',
        'icon'  => 'el el-home'
    ) );

	//**header section start**//
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Header Setting', 'law' ),
        'id'         => 'general-header-setting',
        'subsection' => true,
        'fields'     => array(
			
           array(
                'id'       => 'law-logo',
                'type'     => 'media',
                'title'    => esc_html__( 'Logo Upload','law'),
                'compiler' => 'true',
                'desc'     => esc_html__( 'Logo size 61 X 41 px.','law'),
                'subtitle' => esc_html__( 'Upload Logo for Website','law'),
				'default'  => array( 'url' => get_template_directory_uri().'/images/logo.png'),
            ),
			array(
				'id'       => 'logo-width',
				'type'     => 'text',
				'title'      => esc_html__( 'Logo Width','law'),
			
			),
			array(
                'id'=>'color-background',
                'type'     => 'color',
                'title'    => __('Body Background Color', 'law'), 
                'subtitle' => __('Pick a background color for the theme (default: #fdecee).', 'law'),
                'default'   => array(
                    'color'     => '#646fc0',
                    'alpha'     => 0.8
                ),
                'validate' => 'color',
            ),
		  array(
                'id'       => 'law-sub-header-img',
                'type'     => 'media',
                'title'    => esc_html__( 'Sub Hader Background Image Upload','law'),
                'subtitle' => esc_html__( 'Upload Background Image For Sub Header','law'),
                'desc'     => esc_html__( 'Background IMG Size 1600 X 436 px','law'),
				'default'  => array( 'url' => get_template_directory_uri().'/images/blog.jpg'),
            ),
			array(
                'id'       => 'law-pre-loader',
                'type'     => 'switch',
                'title'    => esc_html__( 'Pre Loader','law'),
                'subtitle' => esc_html__( 'Upload Img For Sub Header','law'),
				'default'  => true,
            ),
			array(
				'id'=>'topbar',
				'type' => 'switch',
				'title' => esc_html__('Show TopBar', 'law'),
				'default'  => false,
			),
			
			array(
				'id'=>'topbar-section',
				'type' => 'section',
				'title' => esc_html__('Top Bar Section', 'law'),
				'required' => array( 'topbar', '=', true ),
				'indent' => true // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'        => 'color-topbar',
				'type'      => 'color_rgba',
				'title'     => 'TopBar  (RGBA Color Picker)',
				'subtitle'  => 'Set color and alpha channel',
				'desc'      => 'The caption of this Background changed to whatever you like!',
				'required' => array( 'topbar', '=', true ),
				'default'   => array(
					'color'     => '#646fc0',
					'alpha'     => 0.8
				),
			),
			array(
				'id'=>'top-contact',
				'type' => 'text',
				'title' => esc_html__('Contact Number', 'law'),
				'required' => array('topbar', '=', true ),
                'default'   => "+ 0920 2730 007 "
                
			),
			array(
				'id'=>'top-email',
				'type' => 'text',
				'title' => esc_html__('Email', 'law'),
				'required' => array('topbar', '=', true ),
                'default'   => "mail@dmail.com",
				'indent' => true // Indent all options below until the next 'section' option is set.
			),
			array(
                'id'       => 'law-social-link',
                'type'     => 'multi_text',
                'title'    => esc_html__('Add Multiple Social Link','law'),
				'required' => array('topbar', '=', true ),
				'desc' => esc_html__( 'Give valid social URL Example  http://facebook.com/yourlink','law'),
            ),
            array(
                'id'        => 'color-button',
                'type'      => 'color_rgba',
                'title'     => 'Button  (RGBA Color Picker)',
                'subtitle'  => 'Set color and alpha channel',
                'desc'      => 'The caption of this Button Background Color!',
                'required' => array( 'topbar', '=', true ),
                'default'   => array(
                    'color'     => '#c53746',
                    'alpha'     => 0
                ),
            ),
            array(
                'id'=>'heading_colorpicker',
                'type'     => 'color',
                'title'    => __('heading Color', 'law'), 
                'subtitle' => __('Pick a background color for the theme (default: #404040).', 'law'),
                'default'  => '#404040',
                'validate' => 'color',
            ),
            array(
                'id'=>'text_colorpicker',
                'type'     => 'color',
                'title'    => __('heading Color', 'law'), 
                'subtitle' => __('Pick a background color for the theme (default: #404040).', 'law'),
                'default'  => '#404040',
                'validate' => 'color',
            ),
        )
    ) );
	//**header section end**//


	//**footer section start**//
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Footer Setting', 'law' ),
        'id'         => 'general-footer-setting',
        'subsection' => true,
        'fields'     => array(
           array(
                'id'       => 'footer-logo',
                'type'     => 'media',
                'title'    => esc_html__( ' Footer Logo Upload','law'),
                'compiler' => 'true',
                'desc'     => esc_html__( 'Logo size 61 X 41 px.','law'),
                'subtitle' => esc_html__( 'Upload Logo for Website','law'),
				'default'  => array( 'url' => get_template_directory_uri().'/images/footer-logo.png'),
            ),
			array(
                'id'       => 'law-footer-sh-desc',
                'type'     => 'editor',
                'title'    => esc_html__( 'Footer Short Description','law'),
            ),
			array(
                'id'       => 'law-footer-social-link',
                'type'     => 'multi_text',
                'title'    => esc_html__( 'Add Multiple Social Link','law'),
				'desc' => __( 'Give valid social URL Example  http://facebook.com/yourlink','law'),
            ),
			array(
                'id'       => 'law-copyright',
                'type'     => 'textarea',
                'title'    => esc_html__( 'Write Copy Right','law'),
				'default'  => 'ThemeBeer. All rights reserved.'
            ),
        )
    ) );
	//**footer section end**//
	
	Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Home Page Setting', 'law'),
        'id'    => 'home-page-setting',
        'icon'  => 'el el-home'
    ) );
	 Redux::setSection( $opt_name, array(
		'title' => esc_html__( 'Introduction','law'),
		'id'    => 'home-introduction',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'introduction',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show / hide Introduction Section ','law'),
				'default'  => false,
			),
		   array(
				'id'       => 'intro-heading',
				'type'     => 'text',
				'title'    => esc_html__( 'Introduction Title','law'),
				'subtitle' => esc_html__( 'Set Introduction Title ','law'),
				'required' => array( 'introduction', '=', true ),
				'default'  => esc_html__('We Make Sure','law'),
			),
			array(
                'id'       => 'intro-descr',
                'type'     => 'editor',
                'title'    => esc_html__( 'Write Something About law','law'),
				'required' => array( 'introduction', '=', true ),
				'default'  => esc_html__('On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble','law'),
            ),
		   array(
                'id'       => 'intro-signature',
                'type'     => 'media',
                'title'    => esc_html__( 'Signature','law'),
				'required' => array( 'introduction', '=', true ),
				'default'  => array( 'url' => get_template_directory_uri().'/images/signature.png'),
				
            ),
		)
    ) );
	
	//**slider selection end
	Redux::setSection( $opt_name, array(
		'title' => esc_html__( 'Service Section','law'),
		'id'    => 'home-service',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'service',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show / hide Service  Section ','law'),
        'subtitle' => __( 'Show/Hide service section on home page','law'),
				'default'  => false,
			),
		)
    ) );
	
	//**footer section start**//el-tasks
	 Redux::setSection( $opt_name, array(
		'title' => esc_html__( 'achievements','law'),
		'id'    => 'home-achievements',
		'subsection' => true,
		'fields'     => array(
			
			array(
                'id'       => 'achievments',
                'type'     => 'switch',
                'title'    => esc_html__( 'Show / Hide Achievements Section ','law'),
				'default'  => false,
            ),
			array(
                'id'       => 'achiev-contact-link',
                'type'     => 'text',
                'title'    => esc_html__( 'Contact Page Link','law'),
				'default'  => esc_html__('#','law'),
				'required' => array( 'achievments', '=', true ),
            ),
		   array(
                'id'       => 'achiev-bac-img',
                'type'     => 'media',
                'title'    => esc_html__( 'Background Image','law'),
				'required' => array( 'achievments', '=', true ),
				'default'  => array( 'url' => get_template_directory_uri().'/images/counter.jpg'),
				
            ),
			array(
				'id'=>'achievements-section',
				'type' => 'section',
				'title' => esc_html__('Achievements Section', 'law'),
				'required' => array( 'achievments', '=', true ),
				'indent' => true // Indent all options below until the next 'section' option is set.
			),
			array(
                'id'       =>'first-achievements',
                'type'     => 'sortable',
				'mode'     => 'text',
                'title'       => esc_html__( 'First Achievements','law'),
				'required' => array( 'achievments', '=', true ),
				'options'  => array(
					'f-achiv-name' => 'Achievements Name',
					'f-achiv-value' => 'Total Achievements This Sector',
				)
            ),
			array(
                'id'       =>'second-achievements',
                'type'     => 'sortable',
				'mode'     => 'text',
                'title'       => esc_html__( 'Second Achievements','law'),
				'required' => array( 'achievments', '=', true ),
				'options'  => array(
					'se-achiv-name' => 'Achievements Name',
					'se-achiv-value' => 'Total Achievements This Sector',
				)
            ),
			array(
                'id'       =>'third-achievements',
                'type'     => 'sortable',
				'mode'     => 'text',
                'title'       => esc_html__( 'Third Achievements','law'),
				'required' => array( 'achievments', '=', true ),
				'options'  => array(
					'th-achiv-name' => 'Achievements Name',
					'th-achiv-value' => 'Total Achievements This Sector',
				)
            ),
			array(
                'id'       =>'fourth-achievements',
                'type'     => 'sortable',
				'mode'     => 'text',
                'title'       => esc_html__( 'Fourth Achievements','law'),
				'required' => array( 'achievments', '=', true ),
				'options'  => array(
					'fo-achiv-name' => 'Achievements Name',
					'fo-achiv-value' => 'Total Achievements This Sector',
				)
            ),
			
		)
    ) );
     Redux::setSection( $opt_name, array(
        'title' => __( 'About Section','law'),
        'id'    => 'home-about',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'about',
                'type'     => 'switch',
                'title'    => esc_html__( 'Show / hide About Law Section ','law'),
                'default'  => false,
            ),
        )
    ) );
	Redux::setSection( $opt_name, array(
		'title' => __( 'Case Studies Section','law'),
		'id'    => 'home-case-studies',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'case-studies',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show / hide Case Studies Section ','law'),
				'default'  => false,
			),
			array(
				'id'        => 'color-case-studies',
				'type'      => 'color_rgba',
				'required' => array( 'case-studies', '=', true ),
				'title'     => 'Case Studies Hover Color (RGBA Color Picker)',
				'subtitle'  => 'Set color and alpha channel',
				'desc'      => 'The caption of this button may be changed to whatever you like!',
				'default'   => array(
					'color'     => '#fdfdfd',
					'alpha'     => 0.7
				),
			),
			array(
				'id'       => 'case-studies-heading',
				'type'     => 'text',
				'title'    => esc_html__( 'Case Studies Heading','law'),
				'default'  => 'Legal Practice',
				'required' => array( 'case-studies', '=', true ),
			),
			array(
				'id'       => 'case-studies-portfolio',
				'type'     => 'text',
				'title'    => esc_html__( 'Case Studies Portfolio','law'),
				'default'  => ' ',
				'required' => array( 'case-studies', '=', true ),
			),
			
		)
    ) );
	Redux::setSection( $opt_name, array(
		'title' => esc_html__( 'Blog Section','law'),
		'id'    => 'home-blog',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'blog',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show / hide Blog Section ','law'),
				'default'  => false,
			),
			array(
				'id'        => 'color-testimonials',
				'type'      => 'color_rgba',
				'title'     => 'Testimonials (RGBA Color Picker)',
				'subtitle'  => 'Set color and alpha channel',
				'required' => array( 'blog', '=', true ),
				'desc'      => 'The caption of this button may be changed to whatever you like!',
				'default'   => array(
					'color'     => '#fdfdfd',
					'alpha'     => 1
				),
			),
		)
    ) );
	
	
	
	
	
	
	Redux::setSection( $opt_name, array(
		'title' => esc_html__( 'Promo','law'),
		'id'    => 'home-promo',
		'subsection' => true,
		'fields'     => array(
		   array(
				'id'       => 'promo',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show / Hide Promo Section','law'),
				'default'  => false,
			),
			array(
				'id'        => 'color-promo',
				'type'      => 'color_rgba',
				'title'     => 'Promo Contant (RGBA Color Picker)',
				'subtitle'  => 'Set color and alpha channel',
				'desc'      => 'The caption of this button may be changed to whatever you like!',
				'required' => array( 'promo', '=', true ),
				'default'   => array(
					'color'     => '#fdfdfd',
					'alpha'     => 0.8
				),
			),
		   array(
				'id'       => 'promo-heading',
				'type'     => 'text',
				'title'    => esc_html__( 'Promo Heading','law'),
				'default'  => esc_html__('Something More','law'),
				'required' => array( 'promo', '=', true ),
			),
			array(
                'id'       => 'promo-descr',
                'type'     => 'editor',
                'title'    => esc_html__( 'Promo Description','law'),
				'default'  => esc_html__('On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble','law'),
				'required' => array( 'promo', '=', true ),
            ),
		   array(
                'id'       => 'promo-img',
                'type'     => 'media',
                'title'    => esc_html__( 'Promo Background Image','law'),
				'default'  => array( 'url' => get_template_directory_uri().'/images/promo.jpg'),
				
				
            ),
			
		)
    ) );
	
	
	Redux::setSection( $opt_name, array(
		'title' => esc_html__( 'Client Section ','law'),
		'id'    => 'home-client',
		'subsection' => true,
		'fields'     => array(
		    array(
				'id'       => 'client',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show / Hide Client Section','law'),
				'default'  => false,
			),
		   array(
				'id'       => 'client-logo',
				'type'     => 'slides',
				'title'    => esc_html__( 'Promo Heading','law'),
				'default'  => esc_html__('Something More','law'),
				'required' => array( 'client', '=', true ),
				'placeholder' => array(
                    'title'       => esc_html__( 'Client Name', 'law' ),
                    'url'         => esc_html__( 'Client site link', 'law' ),
                ),
				'show' => array(
                    'title'       =>true,
                    'subtitle'    =>false,
                    'description' =>false,
                    'url'         => true,
                ),
			),
		)
    ) );
	
	Redux::setSection( $opt_name, array(
		'title' => __( 'Team Section','law'),
		'id'    => 'home-team',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'team',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show / hide Team Section','law'),
				'default'  => false,
			),
			array(
				'id'       => 'team-heading',
				'type'     => 'text',
				'title'    => esc_html__( 'Team Heading','law'),
				'default'  => 'Expert Team',
				'required' => array( 'team', '=', true ),
			),
			array(
                'id'=>'color-team',
                'type'     => 'color',
                'title'    => __('Team Background Color', 'law'), 
                'subtitle' => __('Pick a background color for the theme (default: #fdf7f7).', 'law'),
                'default'  => '#fdf7f7',
                'validate' => 'color',
				'required' => array( 'team', '=', true ),
            ),
			array(
                'id'=>'color-team-hover',
                'type'     => 'color',
                'title'    => __('Team Hover Color', 'redux-framework-demo'), 
                'subtitle' => __('Pick a background color for the theme (default: #fdf7f7).', 'law'),
                'default'  => '#fdf7f7',
                'validate' => 'color',
				'required' => array( 'team', '=', true ),
            ),
		)
    ) );
	
	
	
	Redux::setSection( $opt_name, array(
		'title' => esc_html__( 'Contact Section','law'),
		'id'    => 'home-contact',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'show-contact',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Contact Section','law'),
				'default'  => false,
			),
			array(
                'id'       => 'home-contact-heading',
                'type'     => 'text',
                'title'    => esc_html__('Heading Contact Section','law'),
                'required' => array( 'show-contact', '=', true )
            ),
			array(
                'id'       => 'home-contact-shortcode',
                'type'     => 'text',
                'title'    => 'Contact From Short Code For Home Page',
                'required' => array( 'show-contact', '=', true )
            ),
			array(
                'id'       => 'home-contact-bg',
                'type'     => 'media',
                'title'    => 'Contact Section Background Img',
                'required' => array( 'show-contact', '=', true ),
				'default'  => array( 'url' => get_template_directory_uri().'/images/contact.jpg'),
            ),
			array(
                'id'       => 'home-contact-img',
                'type'     => 'media',
                'title'    => 'Contact Section Right Image',
                'required' => array( 'show-contact', '=', true ),
				'default'  => array( 'url' => get_template_directory_uri().'/images/contact-right.png'),
            ),
		)
    ) );
	
	
	


	Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Contact Option ','law'),
        'id'         => 'contact',
		'icon'  	 => 'el el-network',
        'fields'     => array(
			array(
				'id'       => 'from-page-heading',
				'type'     => 'text',
				'title'      => esc_html__( 'Contact Page Heading','law'),
			),
			array(
				'id'       => 'from-desc',
				'type'     => 'editor',
				'title'      => esc_html__( 'Contact  Page Description','law'),
			),
			array(
				'id'       => 'from-field-heading',
				'type'     => 'text',
				'title'      => esc_html__( 'Contact Message Heading','law'),
			),
			array(
				'id'       => 'map-api-key',
				'type'     => 'text',
				'title'      => esc_html__( 'Api Key For Google Map','law'),
			),
			array(
				'id'       => 'map-latitude',
				'type'     => 'text',
				'title'      => esc_html__( 'Latitude','law'),
			),
			array(
				'id'       => 'map-longitude',
				'type'     => 'text',
				'title'      => esc_html__( 'Longitude','law'),
			),
			array(
				'id'       => 'contact-form',
				'type'     => 'text',
				'title'      => esc_html__( 'Contact From ShortCode','law'),
			),
        )
    ) );
	

    /*
     * <--- END SECTIONS
     */

    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    function compiler_action( $options, $css, $changed_values ) {
        echo '<h1>The compiler hook has run!</h1>';
        echo "<pre>";
        print_r( $changed_values ); // Values that have changed since the last save
        echo "</pre>";
        //print_r($options); //Option values
        //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $return['error'] = $field;
                $field['msg']    = 'your custom error message';
            }

            if ( $warning == true ) {
                $return['warning'] = $field;
                $field['msg']      = 'your custom warning message';
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }
	
	        function change_defaults($defaults) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }

        // Remove the demo link and the notice of integrated demo from the redux-framework plugin
        function remove_demo() {

            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
            }
        }


