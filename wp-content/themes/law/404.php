<?php 
get_header();
?>
	<div class="main-wrap section-padding">
        <div class="page-content blog-single">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                       <article  id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="error_404">
							   <span class="error_code"><?php echo esc_html__('404','law'); ?></span>
							   <span class="error_text"><?php echo esc_html__('Página não encontrada','law'); ?></span>
							   <a href="<?php echo esc_url(home_url('/')); ?>"><?php echo esc_html__('Back to HomePage','law') ?></a>
							</div>
						</article>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();