<?php

/**
 * BBLaw Themes functions and definitions
 *
 * Sets up the theme and provides some helper functions, which are used
 * in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *

 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook.
 *
 * For more information on hooks, actions, and filters, @link http://codex.wordpress.org/Plugin_API
 *
 * @package WordPress
 * @subpackage BBLaw_Themes
 * @since BBLaw Themes 1.0
 */

define('LAW', wp_get_theme()->get( 'Name' ));
define('LAWCSS', get_template_directory_uri().'/css/');
define('LAWJS', get_template_directory_uri().'/js/');
define('LAWIMG', get_template_directory_uri().'/images/');


define( 'LAW_REDUX_ACTIVED', in_array( 'redux-framework/redux-framework.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );
define( 'LAW_METABOX_ACTIVED', in_array( 'meta-box/meta-box.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );
define( 'LAW_CONTACT_ACTIVED', in_array( 'contact-form-7/wp-contact-form-7.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );
define( 'LAW_POST_ACTIVED', in_array( 'law-post-type/law-post-type.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );




require_once get_template_directory().'/lib/wp_bootstrap_navwalker.php';
/*
include  custom widget 
*/
// require_once get_template_directory().'/lib/law_contact_widget.php';
// require_once get_template_directory().'/lib/law_about_widget.php';
// require_once get_template_directory().'/lib/law_aboutus_widget.php';
/*
 tgm plugin 
*/


require_once get_template_directory().'/lib/law_add_plugin_setting.php';


/*
include redux framwork 
*/
if(LAW_METABOX_ACTIVED){
	require_once get_template_directory().'/lib/metabox.php';
}

if(LAW_REDUX_ACTIVED){
	
	require_once get_template_directory().'/lib/options.php';
}
/*
include law custom function
*/
require_once get_template_directory().'/lib/law_custom_function.php';

	
// /*-------------------------------------------*
//  *				Excerpt Length
//  *------------------------------------------*/
if(!function_exists('law_new_excerpt_more')){
	function law_new_excerpt_more( $more ) {
		return '';
	}
	add_filter('excerpt_more', 'law_new_excerpt_more');
}
	
if(!function_exists('law_custom_excerpt_length')){
	function law_custom_excerpt_length($length) {
		global $post;
		if ($post->post_type == 'post')
			return 50;
		else if ($post->post_type == 'slider')
			return 20;
		else
			return 60;
	}
    add_filter('excerpt_length', 'law_custom_excerpt_length');
}
if(!function_exists('law_search_form')){	
	function law_search_form( $form ="" ) {

		$form = sprintf('<form action="%s" id="searchForm" method="post" class="search-form">
					<input type="text" class="sb-search-input"  value="%s" required name="s" placeholder="%s" id="search">
					<button class="sb-search-submit" type="submit"><i class="fa icon_search"></i></button>
				</form>',esc_url( home_url('/')),esc_attr( get_search_query()),esc_attr__('Type and Hit Enter','madison'));
			return $form;
		
       
	}
	add_filter( 'get_search_form','law_search_form');
}
	/*
	law custom pagination 
	*/

/*-------------------------------------------*
 *				Law Setup
 *------------------------------------------*/
if(!function_exists('law_setup')):
	add_filter( 'wp_title', 'baw_hack_wp_title_for_home' );
	function baw_hack_wp_title_for_home( $title )
	{
		if( empty( $title ) && ( is_home() || is_front_page() ) ) {
			return esc_html__( 'Home', 'law' ) . ' | ' . get_bloginfo( 'description' );
		}else{
			return get_bloginfo('name').' | '.$title;
		}
	}
endif;
if(!function_exists('law_setup')):

	function law_setup()
	{
		//Textdomain
		load_theme_textdomain( 'law', get_template_directory() . '/languages' );
	
		add_theme_support( 'post-formats', array( 'aside','audio','gallery','image','link','quote','video' ) );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form' ) );
		add_theme_support( 'automatic-feed-links' );
        add_theme_support('post-thumbnails');

        //*set image size *//
		add_image_size('law-post-thumbnil',802,445,true);
		add_image_size('law-post-small-thumbnil',358,238,true);
		add_image_size('law-slider-thumbnail',1600,800,true);
		add_image_size('law-thumbnail',290,435,true);
		add_image_size('law-case-small-thumbnail',292,230,true);
		add_image_size('law-team-thumbnail',270,270,true);
		add_image_size('law-case-bref-history',400,266,true);
        register_nav_menus( array(
            'primary' => esc_html__('Primary Menu','law')
        ) );

		add_editor_style('');

		if ( ! isset( $content_width ) )
		$content_width = 660;
	}

	add_action('after_setup_theme','law_setup');

endif;
	//default nav menu 
if(!function_exists('law_default_nav_menu')):
	function law_default_nav_menu(){
		echo '<ul class="nav navbar-nav">
				<li class="active"><a href="'.esc_url(home_url('/')).'">'.esc_html__(' ','law').'</a></li>
			  </ul>';
	}
endif;


/*-------------------------------------------*
 *		law Style
 *------------------------------------------*/
add_theme_support( 'menus' );
add_theme_support( 'custom-header' );
add_theme_support( 'custom-background' );
add_theme_support( 'title-tag' );

if(!function_exists('law_style')):

    function law_style(){
    	global $law_options;


        wp_enqueue_style('law',get_stylesheet_uri());
		wp_enqueue_style( 'bootstrap', LAWCSS . 'bootstrap.min.css', array() );
		wp_enqueue_style( 'elegantIcon', LAWCSS . 'elegantIcon.css', array() );
		wp_enqueue_style( 'owl-carousel', LAWCSS . 'owl.carousel.css', array() );
		wp_enqueue_style( 'owl-transitions', LAWCSS . 'owl.transitions.css', array() );
		wp_enqueue_style( 'animate', LAWCSS . 'animate.css', array() );
		wp_enqueue_style( 'style', LAWCSS . 'style.css', array() );
		wp_enqueue_style( 'responsive', LAWCSS . 'responsive.css', array() );
		wp_enqueue_style( 'law-custom-style', LAWCSS . 'custom_style.css', array() );
		
      


	

        wp_enqueue_script('bootstrap',LAWJS.'bootstrap.min.js',array('jquery'));
        wp_enqueue_script('owl-carousel',LAWJS.'owl.carousel.js',array('jquery'));
        wp_enqueue_script('wow-min',LAWJS.'wow.min.js',array('jquery'));
        wp_enqueue_script('jquery.mixitup',LAWJS.'jquery.mixitup.js',array('jquery'));
        wp_enqueue_script('law-custom',LAWJS.'custom.js',array('jquery'));
        wp_enqueue_script('law-google-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAmiJjq5DIg_K9fv6RE72OY__p9jz0YTMI', array('jquery'));
        
        
        //reply comments
        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
            wp_enqueue_script( 'comment-reply' );
        }
	}

    add_action('wp_enqueue_scripts','law_style');

endif;

// filter body class
add_filter('body_class', 'law_body_classes');
if(!function_exists('law_body_classes')){
	function law_body_classes($classes) {
		if (($key = array_search('date', $classes)) !== false || ($key = array_search('search', $classes)) !== false ) {
			unset($classes[$key]);
		}
		 return $classes;
	}
}
if(!function_exists('law_custom_heading')):
	function law_custom_heading(){
		if(is_home()){
			echo esc_html__('Blog','law').'<strong class="red">.</strong>';
		}elseif(is_404()){
			echo '404<strong class="red">.</strong>';
		}elseif(is_search()){
			echo 'Busca<strong class="red">.</strong>';
		}else{
			echo get_the_title().'<strong class="red">.</strong>';
		}
	}
endif;
if(!function_exists('law_admin_style')):

	function law_admin_style()
	{
		if(is_admin())
		{
			wp_register_script('lawpostmeta', get_template_directory_uri() .'/js/post-meta.js');
			wp_enqueue_script('lawpostmeta');
		}
	}

	add_action('admin_enqueue_scripts','law_admin_style');

endif;





    //comment function

	if(!function_exists('law_comments')){
		function law_comments($comment,$args,$depth){
			$GLOBALS['comment'] = $comment;
			extract($args, EXTR_SKIP);

			?>
				<li class="comment parents" id="comment-<?php comment_ID(); ?>">
					<article class="comment-body bc-light">
                        <div class="comment-meta">
					        <?php  echo get_avatar( $comment,80,null,null,array('class'=>array('img-circle'))); ?>
						</div><!-- /.comment-meta -->

						<div class="comment-metadata">
							<h3>
	                            <?php echo get_comment_author_link(); ?>
	                        </h3>
							<span>
					            <?php echo get_comment_date().' '. get_comment_time(); ?>							
							</span>
						</div><!-- /.comment-metadata -->
						<div class="comment-data">
							<div class="comment-content">
								<?php comment_text(); ?>
							</div><!-- .comment-content -->
							<span class="reply">
								<?php comment_reply_link( array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'],'reply_text'=>'Reply' ) ) ); ?>
								
							</span><!-- /.reply --> 
						</div>
					</article><!-- /.comment-body -->
			<?php
		}
	}



//register widget
if(!function_exists('law_widdget_init')):

	function law_widdget_init()
	{
		register_sidebar(
			array( 			
				'name' 			=> esc_html__( 'category Sidebar', 'law' ),
				'id' 			=> 'category',
				'class'         => 'categories',
				'description' 	=> esc_html__( 'Widgets in this area will be shown on Sidebar.', 'law' ),
				'before_title' 	=> '<h3>',
				'after_title' 	=> '<strong class="red">.</strong></h3>',
				'before_widget' => '<aside class="%2$s" >',
				'after_widget' 	=> '</aside>'
			)
		);
		register_sidebar(
			array( 			
				'name' 			=> esc_html__( 'Contact Sidebar', 'law' ),
				'id' 			=> 'contact',
				'class'         => 'categories',
				'description' 	=> esc_html__( 'Widgets in this area will be shown on Sidebar.', 'law' ),
				'before_title' 	=> '<h3>',
				'after_title' 	=> '<strong class="red">.</strong></h3>',
				'before_widget' => '<aside class="%2$s">',
				'after_widget' 	=> '</aside>'
			)
		);

		register_sidebar(
			array( 			
				'name' 			=> esc_html__( 'Footer First Sidebar', 'law' ),
				'id' 			=> 'footer-first',
				'class'         => ' ',
				'description' 	=> esc_html__( 'Widgets in this area will be shown on Sidebar.', 'law' ),
				'before_title' 	=> '<h3>',
				'after_title' 	=> '<strong class="red">.</strong></h3>',
				'before_widget' => '<div class="footer-left law-footer"> ',
				'after_widget' 	=> '</div>'
			)
		);
		register_sidebar(
			array( 			
				'name' 			=> esc_html__( 'Footer Second Sidebar', 'law' ),
				'id' 			=> 'footer-second',
				'class'         => ' ',
				'description' 	=> esc_html__( 'Widgets in this area will be shown on Sidebar.', 'law' ),
				'before_title' 	=> '<h3>',
				'after_title' 	=> '<strong class="red">.</strong></h3>',
				'before_widget' => '<div class="col-sm-4 law-footer">',
				'after_widget' 	=> '</div>'
			)
		);

	}
	
	add_action('widgets_init','law_widdget_init');

endif;


//set background image
if(!function_exists('codepassenger_Hex2RGB')){
	
	function codepassenger_Hex2RGB($color, $opacity=1){
		//echo $color; exit;
		
		$color = str_replace('#', '', $color);
		if (strlen($color) != 6){ return array(0,0,0); }
		$rgb = array();
		for ($x=0;$x<3;$x++){
			$rgb[$x] = hexdec(substr($color,(2*$x),2));
		}

		$output = 'rgba('.$rgb[0].','.$rgb[1].','.$rgb[2].','.$opacity.')';

		return $output;
	}
}


	if(!function_exists('law_background_image_load')):
	function law_background_image_load() {
		
		global $law_option;
				$achievement = get_template_directory_uri().'/images/counter.jpg';
				$subHeaderBack = get_template_directory_uri().'/images/blog.jpg';
				$promo_img = get_template_directory_uri().'/images/promo.jpg';
				$contact_img =  get_template_directory_uri().'/images/contact.jpg';
			if(LAW_REDUX_ACTIVED){
				if(isset($law_option['law-sub-header-img']) && $law_option['law-sub-header-img'] !=''){
					$subHeaderBack = $law_option['law-sub-header-img']['url'];
				}
				if(isset($law_option['achiev-bac-img']) && $law_option['achiev-bac-img'] !=''){
					$achievement = $law_option['achiev-bac-img']['url'];
				}
				if(isset($law_option['promo-img']) && $law_option['promo-img'] !=''){
					$promo_img = $law_option['promo-img']['url'];
				}
				if(isset($law_option['home-contact-bg']) && $law_option['home-contact-bg'] !=''){
					$contact_img = $law_option['home-contact-bg']['url'];
				}if(isset($law_option["achiev-bac-img"]) && $law_option["achiev-bac-img"] !=''){
					$achievement = $law_option["achiev-bac-img"]['url'];
				}
				
				
			}
		
       	
        $custom_css = ".law-page-header	{ background: url($subHeaderBack) no-repeat center center}";
        $custom_css.= ".comment-metadata, .comment-content { margin-left: 0px;}";
        $custom_css.= ".promo{ background: url($promo_img)}";
        $custom_css.= ".contact{ background: url($contact_img)}";
        $custom_css.= ".fact-area{ background-image: url($achievement)}";

        if(LAW_POST_ACTIVED){
	       	$args = array('post_type' => 'slider','posts_per_page'=>5);
	       	$slider = new WP_Query( $args );
	       	if($slider->have_posts()):
	       		$p = 1;
				while ( $slider->have_posts() ) : $slider->the_post();
					if(has_post_thumbnail()){
						$id = get_post_thumbnail_id(get_the_ID());
						$custom_css .= '.header-slide-'.$p.' { background: url('.wp_get_attachment_image_url($id ,"law-slider-thumbnail").')  no-repeat center center}';
		       			$p++;
		       		}
	       		endwhile;
	       	endif;wp_reset_query();
	    }
        
        wp_add_inline_style('law-custom-style',$custom_css );
	}	
	add_action('wp_enqueue_scripts', 'law_background_image_load');
endif;
		//custom widget register
	if(!function_exists('law_custom_color')){
		function law_custom_color(){
			if(LAW_REDUX_ACTIVED){
			
				global $law_option;
				$promo = array();
				$button = array();
				$case_studies = '';
				$testimonials = '';
				$topba = '';
				$promo_color = '';
				$case_color   = '';
				$topbar_color   = '';
				$testimonial_color   = '';
				$testimonial_color_check   =  '';
				$testimonial_color_check_left   = '';
				$brand_nav = ''; 
				$preeloader_color_before = '';
				$preeloader_color_after =  '';
				
				$text_heading = !empty($law_option['heading_colorpicker']) ?  $law_option['heading_colorpicker'] : "#404040";
				$text_colorpicker = !empty($law_option['text_color']) ?  $law_option['text_color'] : "#b1b1b1";
				
				$promo = !empty($law_option['color-promo']) ? $law_option['color-promo']:"";
				
				$case_studies = $law_option['color-case-studies'];
				$testimonials = $law_option['color-testimonials'];
				
				
				$button["color"] = !empty($law_option['color-button']) && is_array($law_option['color-button']) ? $law_option['color-button']['color'] :"#c53746";
				
				$button["opp"] = !empty($law_option['color-button']) ?  codepassenger_Hex2RGB($button["color"],.60) : codepassenger_Hex2RGB("#c53746",.60);
				

				$topbar = $law_option['color-topbar'];
				if(!empty($promo)){
					$promo_color = codepassenger_Hex2RGB($promo['color'],$promo['alpha']);
					$case_color   = codepassenger_Hex2RGB($case_studies['color'],$case_studies['alpha']);
				}
				
				$topbar_color   = !empty($topbar) ? codepassenger_Hex2RGB($topbar['color'],$topbar['alpha']) : "";			
				$testimonial_color   = !empty($testimonials) ? codepassenger_Hex2RGB($testimonials['color'],$testimonials['alpha']) : "" ;
				$testimonial_color_check   =  !empty($testimonials) ? codepassenger_Hex2RGB($testimonials['color'],.4) : "";
				$testimonial_color_check_left   = !empty($testimonials) ?  codepassenger_Hex2RGB($testimonials['color'],.50) : "";
				$brand_nav  = !empty($law_option['color-background']) ? codepassenger_Hex2RGB($law_option['color-background'],.15) : "";
				$brand_nav_brfore  = !empty($law_option['color-background']) ? codepassenger_Hex2RGB($law_option['color-background'],0.35) : "";
				$preeloader_color_before =  !empty($law_option['color-background']) ? codepassenger_Hex2RGB($law_option['color-background'],1) : "" ; 
				$preeloader_color_after =  !empty($law_option['color-background']) ?  codepassenger_Hex2RGB($law_option['color-background'],0.3) : ""; 
				
				
				$custom_css = '.topbar{ background:'.$topbar_color.'}';
				$custom_css .= '.main-nav{ background:'.$law_option['color-background'].'}';
				$custom_css .= '.main-nav:before{ border-right: 14px solid '.$brand_nav_brfore.'}';
				$custom_css .= '.navbar-default .navbar-nav li a:after, .navbar-default .navbar-nav li.active a:after{background:'.$brand_nav_brfore.'}';
				$custom_css .= '.bg-style:before{background: '.$topbar_color.'}';
				$custom_css .= 'input[type=submit],.btn,.btn{background: '.$button["color"].'}';
				$custom_css .= '.sb-search-submit{color: '.$button["color"].'}';
				$custom_css .= '.btn .btn-icon:after{background-color:'.$button["opp"].'}';
				$custom_css .= '.about-laws-tabs .nav-tabs li {border:1px solid '.$brand_nav.'}';
				
				$custom_css .= '.about-laws:before {background-color:'.$brand_nav_brfore.'}';
				$custom_css .= '.red,.social-icons li a i{color:'.$law_option['color-background'].'}';
				$custom_css .= '.about-laws-tabs .nav-tabs > li.active > a, 
								.about-laws-tabs .nav-tabs > li.active > a:hover, 
								.about-laws-tabs .nav-tabs > li.active > a:focus, 
								.about-laws-tabs  .nav>li>a:focus, 
								.about-laws-tabs  .nav>li>a:hover {background-color:'.$brand_nav.'}';
				$custom_css .= '.about-laws-tabs .nav-tabs > li > a:after{border-top:15px solid '.$brand_nav.'}';
				$custom_css .= '.rounded-angle li:before,
								.panel-heading h4 a .icon_minus_alt2,
								.rounded-iconic-accor .panel-title a[aria-expanded=true]:before,
								.testimonial blockquote:before{color:'.$law_option['color-background'].'}';

				$custom_css .= '.testimonial:before{background:'.$testimonial_color_check.';border-left: 12px solid '.$testimonial_color_check_left.';}';
				$custom_css .= '.testimonial{background:'.$testimonial_color_check.';}';
				
				$custom_css .= '.post-date{background:'.$law_option['color-background'].'}';
				$custom_css .= '.promo:before{background:'.$promo_color.';}';
				$custom_css .= '.fantasy-bg{background:'.$law_option['color-team'].';}';
				//$custom_css .= '.member-info a i{color:'.$law_option['color-background'].';}';
				$custom_css .= '.member-info:after{background:'.$law_option['color-team-hover'].';}';
				$custom_css .= '.member-info a i{color:'.$law_option['color-background'].';}';

				$custom_css .= '.side-bar  aside.widget_archive li:before, 
								.side-bar  aside.widget_recent_comments li:before, 
								.side-bar  aside.widget_meta li:before, 
								.side-bar  aside.widget_pages li:before, 
								.side-bar  aside.widget_categories li:before, 
								.side-bar  aside.widget_nav_menu li:before,
								.side-bar aside li a:hover{color:'.$law_option['color-background'].'}';

				$custom_css .= '#pre-loader{background: linear-gradient('.$preeloader_color_before."-40%,".$preeloader_color_after."60%".')}';
				$custom_css .= 'h1, h2, h3, h4, h5, h6 {color: '.$text_heading.'}';
				$custom_css .= 'p {color: '.$text_colorpicker.'}';
				wp_add_inline_style('law-custom-style',$custom_css);
		?>

		
		<?php
			}
		}
		add_action('wp_enqueue_scripts', 'law_custom_color');
	}

	
/*
	include tgm
*/
require_once get_template_directory().'/lib/class-tgm-plugin-activation.php';

function law_register_required_plugins() {

    $plugins = array(
		array(
            'name'               => esc_html__( 'Law Post Type', 'law' ),
            'slug'               => 'law-post-type',
            'source'             => get_stylesheet_directory() . '/plugins/law-post-type.zip',
            'required'           => true,
            'version'            => '1.0.0',
            'force_activation'   => true,
            'force_deactivation' => true,
            'external_url'       => '',
        ),
        // Plugins from the WordPress Plugin Repository.
        array(
            'name'      => esc_html__( 'Contact Form 7', 'law' ),
            'slug'      => 'contact-form-7',
            'required'  => true,
        ),
		array(
            'name'      => esc_html__( 'Redux Framework', 'law' ),
            'slug'      => 'redux-framework',
            'required'  => true,
        ),
		
    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => true,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => esc_html__( 'Install Required Plugins', 'law' ),
            'menu_title'                      => esc_html__( 'Install Plugins', 'law' ),
            'installing'                      => esc_html__( 'Installing Plugin: %s', 'law' ), // %s = plugin name.
            'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'law' ),
            'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.','law' ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.','law'  ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.','law'  ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.','law'  ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.','law'  ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ,'law' ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ,'law' ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ,'law' ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins','law'  ),
            'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins' ,'law' ),
            'return'                          => esc_html__( 'Return to Required Plugins Installer', 'law' ),
            'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'law' ),
            'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', 'law' ), // %s = dashboard link.
            'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );

    tgmpa( $plugins, $config );

}
add_action( 'tgmpa_register', 'law_register_required_plugins' ); 

if(!function_exists('law_web_fonts_url')):
	function law_web_fonts_url($font) {
		$font_url = '';

		if ( 'off' !== _x( 'on', 'Google font: on or off', 'law' ) ) {
			$font_url = add_query_arg( 'family', urlencode($font), "//fonts.googleapis.com/css" );
		}
		return $font_url;
	}
endif;
/*
Enqueue scripts and styles.
*/
if(!function_exists('law_font_scripts')):
	function law_font_scripts() {
		wp_enqueue_style( 'law-web-font', law_web_fonts_url('PT Serif:400,400italic,700italic|Open Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'), array(), '1.0.0' );
		
	}
endif;
add_action( 'wp_enqueue_scripts', 'law_font_scripts' );

	
if ( ! function_exists( 'law_tag_list' ) ) :

function law_tag_list() {
	if ( 'post' == get_post_type() ) {
		$tags_list = get_the_tag_list( '');
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'law' ) . '</span>', $tags_list );
		}
	}
}
endif;

	
	//========= Duplicket post========//

function rd_duplicate_post_as_draft(){
	global $wpdb;
	if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die('No post to duplicate has been supplied!');
	}
 
	/*
	 * get the original post id
	 */
	$post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
	/*
	 * and all the original post data then
	 */
	$post = get_post( $post_id );
 
	/*
	 * if you don't want current user to be the new post author,
	 * then change next couple of lines to this: $new_post_author = $post->post_author;
	 */
	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;
 
	/*
	 * if post data exists, create the post duplicate
	 */
	if (isset( $post ) && $post != null) {
 
		/*
		 * new post data array
		 */
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'draft',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);
 
		/*
		 * insert the post by wp_insert_post() function
		 */
		$new_post_id = wp_insert_post( $args );
 
		/*
		 * get all current post terms ad set them to the new post draft
		 */
		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}
 
		/*
		 * duplicate all post meta just in two SQL queries
		 */
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}
 
 
		/*
		 * finally, redirect to the edit post screen for the new draft
		 */
		wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
		exit;
	} else {
		wp_die('Post creation failed, could not find original post: ' . $post_id);
	}
}
add_action( 'admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft' );
 
/*
 * Add the duplicate link to action list for post_row_actions
 */
function rd_duplicate_post_link( $actions, $post ) {
	if (current_user_can('edit_posts')) {
		$actions['duplicate'] = '<a href="admin.php?action=rd_duplicate_post_as_draft&amp;post=' . $post->ID . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
	}
	return $actions;
}
 
add_filter( 'post_row_actions', 'rd_duplicate_post_link', 10, 2 );


//remobing redux notice
	if ( ! function_exists( 'law_remove_redux_message' ) ) :
	add_action('admin_head', 'law_remove_redux_message');

	function law_remove_redux_message() {
	  print '<style>
		.updated.redux-message.redux-notice{
			display:none;
		}
		.rAds{
			opacity: 0;
		}		
	  </style>';
	}
	endif;

	

	