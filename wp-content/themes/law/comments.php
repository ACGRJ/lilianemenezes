<?php

/**
 * @package WordPress
 * @subpackage law
 */
?>
<?php 
if ( post_password_required() ) {
	return;
}
?>
<div class="comments-area">
    <?php

		if( number_format_i18n(get_comments_number()) > 0 ) {

			$comment_no = number_format_i18n(get_comments_number());
			$comment_no = (!empty($comment_no))  ? "Comentários (&nbsp;".$comment_no."&nbsp;)": ' ';
				
			
	 ?>
			<h2 class="titled-comment-number"><?php print $comment_no; ?><strong class="red">.</strong></h2>
			<ol class="comment-list">
				<?php
					wp_list_comments( array(
						'style'       => 'ol',
						'short_ping'  => true,
						'avatar_size' => 82,
						'callback'    => 'law_comments',
					) );
				?>	
			</ol>
			
	<?php } ?>
	
	<?php if ( ! comments_open() ) : ?>
        <p class="no-comments"><?php esc_html_e( 'Comments are closed','law'); ?></p>
    <?php endif; ?>
    <?php
		$commenter = wp_get_current_commenter();
		$req = get_option( 'require_name_email' );
		$aria_req = ( $req ? " aria-required='true'" : '' );
		$required_text = '  ';
		$args = array(
			'id_form'           => 'commentform',
			'id_submit'         => ' ',
			'class_submit'         => 'btn ',
			'title_reply_to'    => esc_html__( 'Deixe uma resposta para','law'),
            'title_reply'       =>' ',
			'cancel_reply_link' => esc_html__( 'Cancelar resposta','law' ),
			'label_submit'      =>' Enviar 
                                            <strong class="btn-icon">
                                                <span class="arrow_right"></span>     
                                            </strong>',
            
            'comment_notes_before' => '<div class="personal-openion mrt-40"><h2 class="mrb-40">' .
				esc_html__( 'Leave A Comment','law' ) . ( $req ? esc_html($required_text) : '' ) .
			'</h2>',
				
			'comment_field' =>  '<div class= "row"><div class="personal-openion"><div class="col-sm-12">'.
					'<textarea id="comment" class="form-control" name="comment" rows="7" placeholder="'. esc_html__( 'Mensagem','law') .'"></textarea>' .
			'</div></div></div>',

			'fields' => apply_filters( 'comment_form_default_fields', array(
				'author' => '<div class="row"><div class="col-sm-4">' .
					
						'<input name="author" type="text" class="form-control" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . '  placeholder="' . esc_html__( 'Name*','law') . '"/>'.
					
				'</div>',

				'email' => '<div class="col-sm-4">' .
					
						'<input name="email" type="text" class="form-control"  value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' placeholder="' . esc_html__( 'Email*','law') . '" />'.
				'</div>',

				'url' => '<div class="col-sm-4">'.
					
						'<input name="subject" type="text" class="form-control"  value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" placeholder="'. esc_html__( 'Subject','law') .'"/>'.
					
				'</div></div></div>'
			) )
		);

		comment_form($args); ?>

</div>