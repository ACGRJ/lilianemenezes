<?php 
?>
<?php get_header(); ?>
<div class="main-wrap section-padding">
	<div class="page-content">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-8 col-sm-7">
	                <article class="page-item">
                		 <div class="post-part">
	                		 <?php if(has_post_thumbnail()) { ?>
	                		 	<?php the_post_thumbnail('law-post-thumbnil', array('class' => 'img-responsive'));?>
	                        <?php } ?>
                           
                           	<p><?php the_content(); ?></p>
                           
                        </div>
						<?php echo do_shortcode('[key-factors]'); ?>
	                	
	                </article>
	            </div>
	             <div class="col-md-4 col-sm-5">
                    <div class="side-bar">
						<?php dynamic_sidebar('contact'); ?>
					</div>
				</div>
	        </div>
	    </div>
	</div>
</div>


<?php get_footer(); ?>