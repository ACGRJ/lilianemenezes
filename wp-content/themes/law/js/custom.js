(function($){
	$(document).ready(function () {
		'use strict';
		/*===============================
		Search
		===============================*/		
		$('#search-trigger').on('click', function () {
			$('#searchForm').slideToggle();
		});
		
		/*===============================
		Carousel & Slider
		===============================*/
		
		/*** Header slider ***/
		$('#header-slider').owlCarousel({
			singleItem: true,
			autoPlay: 5000,
			slideSpeed: 2000,
			stopOnHover: true,
			pagination: false,
			navigation: true,
			navigationText: ['<i class=\"arrow_carrot-left\"></i>', '<i class=\"arrow_carrot-right\"></i>'],
			transitionStyle: "fade",
		});
		
		/*** Case Carousel ***/
		$('#case-study-carousel').owlCarousel({
			items: 5,
			itemsDesktop: [1199,4],
			itemsDesktopSmall: [991,3],
			itemsTablet: [767, 2],
			itemsMobile: [479, 1],
			slideSpeed: 500,
			autoPlay: true,
			stopOnHover: true,
			pagination: false,
		});
		
		/*** Testimonial Slider ***/
		$('#testimonial-carousel').owlCarousel({
			singleItem: true,
			slideSpeed: 500,
			autoPlay: true,
			stopOnHover: true,
			pagination: false,
		});
		
		/*** Client logo carousel ***/
		$('#client-carousel').owlCarousel({
			items: 5,
			itemsDesktop: [1199,4],
			itemsDesktopSmall: [991,3],
			itemsTablet: [767, 2],
			itemsMobile: [479, 1],
			slideSpeed: 500,
			autoPlay: true,
			stopOnHover: true,
			pagination: false,
		});
		
		/*===============================
		MixitUp Filtering
		===============================*/
		if($('#case-shorted').length > 0){
		   $('#case-shorted').mixItUp(); 
		}
		/*===============================
		Map
		===============================*/
		

	});

//Window Load
	$(window).on('load', function (){
		
		$("#spinner").fadeOut();
		$("#sitePreloader").delay(200).fadeOut("slow");
	});
})(jQuery);