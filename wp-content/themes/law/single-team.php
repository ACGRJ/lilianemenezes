<?php 

get_header(); 
?>
<div class="main-wrap section-padding">
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-7">
                	<article class="law-item">
						<?php
							
							if ( have_posts() ) :
								while ( have_posts() ) : the_post();
							
						?>
						
							<?php if(has_post_thumbnail()){
								the_post_thumbnail('law-post-thumbnil', array('class' => 'img-responsive'));
							} ?>
							<h3><?php echo  esc_html_e('Sobre','law'); ?><strong class="red">.</strong></h3>
							<div id="about-team"><?php echo get_the_content(); ?></div>
							<!--<h3><?php echo esc_html_e('Eduction','law'); ?><strong class="red">.</strong></h3>
							<p><?php echo  rwmb_meta('law_eduction'); ?></p>-->
						
						
						

						<div class="post-part">

							<?php  
								$eduction_level  = rwmb_meta('law_eduction_level');
								if(!empty($eduction_level)){
									foreach($eduction_level as $item){
										
										echo '<p>'.$item.'</p><hr>';
									}
								}
							?>
	 
						</div>
						
						
						<?php 
										  
							endwhile;
						endif; 
					 
					?>
					</article>
				</div>
				<div class="col-md-4 col-sm-5">
                    <div class="side-bar">
						<?php get_sidebar(); ?> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- /.contents --> 
<?php  get_footer(); ?>